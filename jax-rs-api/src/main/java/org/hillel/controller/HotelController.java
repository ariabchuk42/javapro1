package org.hillel.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.HttpStatus;
import org.hillel.Hotel;
import org.hillel.service.HotelService;

@Path("/hotels")
public class HotelController {

  HotelService service = new HotelService();

  @GET
  @Path("/hi")
  public Response hi(@QueryParam("name") String name) {
    return Response.ok("hi " + name).build();
  }

  @GET
  @Path("/hello/{name}")
  public Response hello(@PathParam("name") String name) {
    return Response.ok("hello " + name).build();
  }

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response addHotel(Hotel hotel) {

    return Response.ok(service.addHotel(hotel)).build();
  }

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getHotel(@PathParam("id") UUID id) {
    return Response.ok(service.getHotel(id)).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getHotels() {
    return Response.ok(service.getHotels()).build();
  }

  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  public Response updateHotel(Hotel hotel) {
    return Response.ok(service.updateHotel(hotel)).build();
  }

  @DELETE
  @Path("/{id}")
  public Response deleteHotel(@PathParam("id") UUID id) {
    service.deleteHotel(id);
    return Response.ok().build();
  }

}
