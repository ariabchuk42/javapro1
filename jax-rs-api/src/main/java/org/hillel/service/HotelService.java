package org.hillel.service;

import java.util.Collection;
import java.util.UUID;
import org.hillel.Hotel;
import org.hillel.repository.HotelRepository;

public class HotelService {

  HotelRepository repo = new HotelRepository();

  public Hotel addHotel(Hotel hotel) {
    return repo.addHotel(hotel);
  }

  public Hotel getHotel(UUID id) {
    return repo.getHotel(id);
  }

  public Collection<Hotel> getHotels() {
    return repo.getHotels();
  }

  public Hotel updateHotel(Hotel hotel) {
    return repo.updateHotel(hotel);
  }

  public void deleteHotel(UUID id) {
    repo.deleteHotel(id);
  }

}
