package org.hillel.repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.hillel.Hotel;

public class HotelRepository {

  Map<UUID, Hotel> storage = new HashMap<>();

  public Hotel addHotel(Hotel hotel) {
    hotel.setId(UUID.randomUUID());
    storage.put(hotel.getId(), hotel);
    return hotel;
  }


  public Hotel getHotel(UUID id) {
    return storage.get(id);
  }

  public Collection<Hotel> getHotels() {
    return storage.values();
  }

  public Hotel updateHotel(Hotel hotel) {
    Hotel h = storage.get(hotel.getId());
    h.setName(hotel.getName());
    h.setDescription(hotel.getDescription());
    h.setLang(hotel.getLang());
    h.setRanking(hotel.getRanking());

    return h;
  }


  public void deleteHotel(UUID id) {
    storage.remove(id);
  }


}
