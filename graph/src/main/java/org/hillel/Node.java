package org.hillel;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

@Getter
public class Node {

  private String name;

  Map<Node, Integer> connectedNode;

  public Node(String name) {
    this.name = name;
    connectedNode = new HashMap<>();
  }

  public void connectNode(Node node, Integer distance) {
    connectedNode.put(node, distance);
  }

}
