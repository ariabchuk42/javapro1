package org.hillel;

import java.util.HashMap;
import java.util.Map;
import javafx.util.Pair;
import javax.swing.ImageIcon;
import lombok.AllArgsConstructor;
import lombok.Getter;

public class Dijkstra {

  private Graph graph;

  private Map<Node, NodeDetails> path;

  public Dijkstra(Graph graph) {
    this.graph = graph;
  }

  public Integer findPath(Node from, Node to) {

    this.path = new HashMap<>();
    path.put(from, new NodeDetails(from, 0, false));

    calc(from);

    return path.get(to).getDistance();
  }

  private void calc(Node from) {

    NodeDetails nodeDetails = path.get(from);
    if (nodeDetails.isVisited()) {
      return;
    }

    nodeDetails.visited = true;

    for (Map.Entry<Node, Integer> entry : from.getConnectedNode().entrySet()) {
      if (path.containsKey(entry.getKey())) {
        if (entry.getValue() > nodeDetails.getDistance() + entry.getValue()) {
          path.put(entry.getKey(), new NodeDetails(from, nodeDetails.getDistance() + entry.getValue(), false));
        }
      } else {
        path.put(entry.getKey(), new NodeDetails(from, nodeDetails.getDistance() + entry.getValue(), false));
      }
      calc(entry.getKey());
    }
  }

  @Getter
  @AllArgsConstructor
  private static class NodeDetails {

    Node from;
    Integer distance;
    boolean visited;
  }

}
