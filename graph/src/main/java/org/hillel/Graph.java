package org.hillel;

import java.util.HashSet;
import java.util.Set;

public class Graph {

  public Graph() {
    this.nodes = new HashSet<>();
  }

  private final Set<Node> nodes;

  public void connect(Node a, Node b, Integer distance) {
    a.connectNode(b, distance);
    b.connectNode(a, distance);
    nodes.add(a);
    nodes.add(b);
  }

}
