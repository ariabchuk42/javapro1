package org.hillel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DijkstraTest {

  @Test
  void test() {

    Node a = new Node("A");
    Node b = new Node("B");
    Node c = new Node("C");
    Node d = new Node("D");
    Node e = new Node("E");
    Node f = new Node("F");

    Graph graph = new Graph();
    graph.connect(a, b, 5);
    graph.connect(b, c, 3);
    graph.connect(c, d, 4);
    graph.connect(d, e, 9);
    graph.connect(e, f, 1);
    graph.connect(a, f, 4);
    graph.connect(a, c, 12);

    Dijkstra dijkstra = new Dijkstra(graph);



//    Assertions.assertEquals(12, dijkstra.findPath(a, d));
//    //Assertions.assertEquals(12, dijkstra.findPath(c, f));
//    Assertions.assertEquals(10, dijkstra.findPath(b, e));
//    Assertions.assertEquals(10, dijkstra.findPath(d, f));

  }
}
