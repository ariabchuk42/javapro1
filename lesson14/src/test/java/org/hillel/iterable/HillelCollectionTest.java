package org.hillel.iterable;

import java.util.Collection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class HillelCollectionTest {

  static Collection<Car> carList;

  @BeforeAll
  static void init() {

    Car bmw = Car.builder()
        .model("BMW")
        .cost(125.8)
        .build();
    Car toyota = Car.builder()
        .model("Toyota")
        .cost(225.8)
        .build();
    Car jeep = Car.builder()
        .model("Jeep")
        .cost(192.6)
        .build();
    carList = new HillelCollection<>();
    carList.add(bmw);
    carList.add(toyota);
    carList.add(jeep);
  }

  @Test
  void collectionForEachTest() {
    for (Car car : carList) {

      System.out.println(car.toString());
    }

    Assertions.assertEquals(3, carList.size());

  }

  @Test
  void collectionClearTest() {

    System.out.println("Clearing Hillel Collection");
    carList.clear();

    Assertions.assertEquals(0, carList.size());
  }
}
