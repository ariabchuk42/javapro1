package org.hillel.iterable;

import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CarServiceTest {

  @Test
  void testIterable() {

    Car bmw = Car.builder()
        .model("BMW")
        .cost(125.8)
        .build();
    Car toyota = Car.builder()
        .model("Toyota")
        .cost(225.8)
        .build();
    Car jeep = Car.builder()
        .model("Jeep")
        .cost(192.6)
        .build();

    Car[] cars = {bmw, toyota, jeep};

    CarService carService = CarService.builder()
        .name("Vidi")
        .cars(cars)
        .build();

    for (Car car : carService) {

      System.out.println(car.toString());
    }

    Assertions.assertIterableEquals(Arrays.asList(cars), carService);
  }
}
