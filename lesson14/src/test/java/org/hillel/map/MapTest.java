package org.hillel.map;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import org.junit.jupiter.api.Test;

public class MapTest {

  @Test
  void testMap() {

    Map<Integer, String> map = new HashMap<>();
    map.put(2, "second");
    map.put(3, "kolobok");

    System.out.println(map.get(2));
    System.out.println(map.get(5));

    map.put(null, "null value");
    System.out.println(map.get(null));

    System.out.println("-------------------");

    for (Map.Entry pair: map.entrySet()) {
      System.out.println("key: " + pair.getKey() + ", value: " + pair.getValue());
    }

    System.out.println("-------------------");
    System.out.println(map.containsKey(2));

    map.forEach((k, v) -> System.out.println("key: " + k + ", value: " + v));

    System.out.println("-------------------");
    System.out.println(map.getOrDefault(5, "fifth"));
  }


  @Test
  void mapRemove() {

    Map<Integer, String> map = new HashMap<>();
    map.put(2, "second");
    map.put(3, "kolobok");

    System.out.println(map.containsKey(2));
    System.out.println(map.remove(2));
    System.out.println(map.containsKey(2));
  }

  @Test
  void mapCompute() {

    Map<Integer, String> map = new HashMap<>();
    map.put(2, "second");
    map.put(3, "kolobok");

    map.forEach((k, v) -> System.out.println("key: " + k + ", value: " + v));
    System.out.println("-------------------");

    map.computeIfAbsent(5, k -> "5");
    map.forEach((k, v) -> System.out.println("key: " + k + ", value: " + v));

    System.out.println("-------------------");
    System.out.println(map.compute(2, (k, v) -> "key: " + k + ", value: " + v));


    System.out.println("-------------------");
    BiFunction<Integer, String, String> compute = (k, v) -> "key: " + k + ", value: " + v;
    System.out.println(map.compute(5, compute));
  }

  @Test
  void testHillelMap() {

    Map<Integer, String> map = new HashMap<>();
    map.put(2, "second");
    map.put(3, "kolobok");

    System.out.println(map.get(2));
    System.out.println(map.get(5));

    map.put(null, "null value");
    System.out.println(map.get(null));

    System.out.println("-------------------");

    for (Map.Entry pair: map.entrySet()) {
      System.out.println("key: " + pair.getKey() + ", value: " + pair.getValue());
    }

    System.out.println("-------------------");
    System.out.println(map.containsKey(2));

    map.forEach((k, v) -> System.out.println("key: " + k + ", value: " + v));

    System.out.println("-------------------");
    System.out.println(map.getOrDefault(5, "fifth"));
  }
}
