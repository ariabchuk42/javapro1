package org.hillel.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;
import org.hillel.iterable.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CollectionTest {


  Deque<Car> carDeque;
  @BeforeEach
  void init() {

    carDeque = new LinkedList<>();
    carDeque.add(Car.builder().model("Toyota").cost(12548.2).build());
    carDeque.add(Car.builder().model("ZAZ").build());
    carDeque.add(Car.builder().model("BMW").cost(21255.2).build());
    carDeque.add(Car.builder().model("Kia").cost(52548.2).build());
    carDeque.add(Car.builder().model("Opel").cost(448.2).build());
    carDeque.add(Car.builder().model("Nissan").cost(54865.2).build());
    carDeque.add(Car.builder().model("Nissan").cost(54865.2).build());
    carDeque.add(Car.builder().model("Nissan").cost(54865.2).build());
    carDeque.add(Car.builder().model("Nissan Juke").cost(54865.2).build());
  }

  @Test
  void testArrayList() {

    List<String> stringList = Arrays.asList("one", "two", "three", "four", "five");

    System.out.println("Is list containing element: " + stringList.contains("two"));

    System.out.println("Classic foreach");
    for (String str : stringList) {
      System.out.println(str);
    }

    System.out.println("Foreach method");
    stringList.forEach(System.out::println);

    stringList.sort(Comparator.naturalOrder());

    System.out.println("Sorted list");
    stringList.forEach(System.out::println);
  }

  @Test
  void testLinkedList() {

    List<String> stringList = new LinkedList<>();
    stringList.add("one");
    stringList.add("two");
    stringList.add("three");
    stringList.add("four");
    stringList.add("five");

    System.out.println("Is list containing element: " + stringList.contains("two"));

    System.out.println("Classic foreach");
    for (String str : stringList) {
      System.out.println(str);
    }
    System.out.println("---------------");

    System.out.println("Foreach method");
    stringList.forEach(System.out::println);

    stringList.sort(Comparator.naturalOrder());
    System.out.println("---------------");

    System.out.println("Sorted list");
    stringList.forEach(System.out::println);

    stringList.add("two");

    stringList.remove("three");
    stringList.remove(0);

    System.out.println("---------------");
    System.out.println("list after manipulations:");
    stringList.forEach(System.out::println);

  }

  @Test
  void testCarList() {

    List<Car> carList = new ArrayList<>();
    carList.add(Car.builder().model("Toyota").cost(12548.2).build());
    carList.add(Car.builder().model("ZAZ").build());
    carList.add(Car.builder().model("BMW").cost(21255.2).build());
    carList.add(Car.builder().model("Kia").cost(52548.2).build());
    carList.add(Car.builder().model("Opel").cost(448.2).build());
    carList.add(Car.builder().model("Nissan").cost(54865.2).build());

    carList.forEach(System.out::println);

    carList.sort(Comparator.comparing(Car::getModel));

    System.out.println("-----------");
    carList.forEach(System.out::println);

  }

  @Test
  void testHashSet() {

    Set<Car> carSet = new HashSet<>();
    carSet.add(Car.builder().model("Toyota").cost(12548.2).build());
    carSet.add(Car.builder().model("ZAZ").build());
    carSet.add(Car.builder().model("BMW").cost(21255.2).build());
    carSet.add(Car.builder().model("Kia").cost(52548.2).build());
    carSet.add(Car.builder().model("Opel").cost(448.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());

    carSet.forEach(System.out::println);

    carSet.removeIf(c -> c.getModel().length() == 3);

    System.out.println("Set after removal");
    carSet.forEach(System.out::println);

  }

  @Test
  void testTreeSet() {

    Set<Car> carSet = new TreeSet<>(Comparator.comparing(Car::getModel));
    carSet.add(Car.builder().model("Toyota").cost(12548.2).build());
    carSet.add(Car.builder().model("ZAZ").build());
    carSet.add(Car.builder().model("BMW").cost(21255.2).build());
    carSet.add(Car.builder().model("Kia").cost(52548.2).build());
    carSet.add(Car.builder().model("Opel").cost(448.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());

    carSet.forEach(System.out::println);

    carSet.removeIf(c -> c.getModel().length() == 3);

    System.out.println("Set after removal");
    carSet.forEach(System.out::println);

  }

  @Test
  void testLinkedHashSet() {

    Set<Car> carSet = new LinkedHashSet<>();
    carSet.add(Car.builder().model("Toyota").cost(12548.2).build());
    carSet.add(Car.builder().model("ZAZ").build());
    carSet.add(Car.builder().model("BMW").cost(21255.2).build());
    carSet.add(Car.builder().model("Kia").cost(52548.2).build());
    carSet.add(Car.builder().model("Opel").cost(448.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());
    carSet.add(Car.builder().model("Nissan").cost(54865.2).build());

    carSet.forEach(System.out::println);

    carSet.removeIf(c -> c.getModel().length() == 3);

    System.out.println("Set after removal");
    carSet.forEach(System.out::println);

  }

  @Test
  void testPeekLinkedCarList() {

    System.out.println("Peek default" + carDeque.peek());
    System.out.println("Peek last" + carDeque.peekLast());
    System.out.println("Peek first" + carDeque.peekFirst());
  }

  @Test
  void testPollLinkedCarList() {

    System.out.println("=======cars init========");
    carDeque.forEach(System.out::println);

    System.out.println("=======poll cars========");
    System.out.println("Poll default" + carDeque.poll());
    System.out.println("Poll last" + carDeque.pollLast());
    System.out.println("Poll first" + carDeque.pollFirst());


    System.out.println("=======cars after poll========");

    carDeque.forEach(System.out::println);

  }

  @Test
  void testPoll2LinkedCarList() {

    Car car = carDeque.poll();
    while (car != null) {
      System.out.println(car);
      car = carDeque.poll();
    }
  }

  @Test
  void testPopLinkedCarList() {

    Assertions.assertThrows(NoSuchElementException.class, this::testQueuePopMethod);
  }

  private void testQueuePopMethod() {
    Car car = carDeque.pop();
    while (car != null) {
      System.out.println(car);
      car = carDeque.pop();
    }
  }

  @Test
  void testLinkedCarList() {
    carDeque.stream()
        .distinct()
        .forEach(System.out::println);
  }

  @Test
  void testConversion() {
    Set<Car> cars = new HashSet<>(carDeque);

    cars.forEach(c -> System.out.println(c.getModel()));

  }

  @Test
  void testSwap() {

    carDeque.forEach(System.out::println);
    Collections.swap((LinkedList) carDeque, 1,3);
    System.out.println("Cars after swap: ");
    carDeque.forEach(System.out::println);
  }
}
