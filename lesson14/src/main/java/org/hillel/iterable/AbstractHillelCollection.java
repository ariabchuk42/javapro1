package org.hillel.iterable;

import java.util.Collection;
import java.util.Iterator;

public abstract class AbstractHillelCollection<T> implements Collection<T> {

  @Override
  abstract public int size();

  @Override
  public boolean isEmpty() {
    return this.size() == 0;
  }

  @Override
  abstract public boolean contains(Object o);

  @Override
  abstract public Iterator<T> iterator();

  @Override
  abstract public Object[] toArray();

  @Override
  public <T1> T1[] toArray(T1[] a) {
    return (T1[]) toArray();
  }

  @Override
  abstract public boolean add(T t);

  abstract public boolean remove(Object o);

  @Override
  public boolean containsAll(Collection<?> c) {
    for (Object val : c) {
      if (!this.contains(val)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    for (T val : c) {
      this.add(val);
    }

    return true;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    for (Object val : c) {
      this.remove(val);
    }

    return true;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    throw new RuntimeException("Method is not implemented");
  }

  @Override
  abstract public void clear();
}
