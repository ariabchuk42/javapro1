package org.hillel.iterable;

import java.util.Iterator;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CarService implements Iterable<Car> {

  private final Car[] cars;
  private final String name;

  @Override
  public Iterator<Car> iterator() {
    return new CarIterator();
  }

  private class CarIterator implements Iterator<Car> {

    int index = 0;

    @Override
    public boolean hasNext() {
      return cars.length > index;
    }

    @Override
    public Car next() {
      return cars[index++];
    }
  }

}
