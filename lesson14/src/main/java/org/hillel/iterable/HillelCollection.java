package org.hillel.iterable;

import java.util.Collection;
import java.util.Iterator;

public class HillelCollection<T> implements Collection<T> {

  private T[] l = (T[]) new Object[1];

  private int size;

  @Override
  public int size() {
    return this.size;
  }

  @Override
  public boolean isEmpty() {
    return this.size == 0;
  }

  @Override
  public boolean contains(Object o) {

    for (T v : l) {
      if (v.equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator<T> iterator() {
    return new HillelIterator();
  }

  @Override
  public Object[] toArray() {
    return l;
  }

  @Override
  public <T1> T1[] toArray(T1[] a) {
    return (T1[]) toArray();
  }

  @Override
  public boolean add(T t) {

    if (l.length == size) {
      T[] temp = l;
      l = (T[]) new Object[this.size() * 2];
      System.arraycopy(temp, 0, l, 0, this.size());
    }
    l[size++] = t;
    return true;
  }

  @Override
  public boolean remove(Object o) {
    throw new RuntimeException("Method is not implemented");
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    for (Object val : c) {
      if (!this.contains(val)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    for (T val : c) {
      this.add(val);
    }

    return true;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    throw new RuntimeException("Method is not implemented");
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    throw new RuntimeException("Method is not implemented");
  }

  @Override
  public void clear() {
    l = (T[]) new Object[1];
    size = 0;
  }

  private class HillelIterator implements Iterator<T> {

    private int index = 0;

    @Override
    public boolean hasNext() {
      return size() > index;
    }

    @Override
    public T next() {
      return l[index++];
    }
  }

}
