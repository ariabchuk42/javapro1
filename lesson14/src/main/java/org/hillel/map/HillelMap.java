package org.hillel.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class HillelMap<K, V> implements Map<K, V> {

  private int CAPACITY = 16;

  private HillelKeyValueBucket<K, V>[] buckets;
  int size = 0;

  public HillelMap() {
    buckets = new HillelKeyValueBucket[CAPACITY];
  }

  private int getHash(Object key) {
    return key.hashCode() % CAPACITY;
  }

  private HilleKeyValueEntry<K, V> getEntry(Object key) {

    int hash = getHash(key);
    for (HilleKeyValueEntry<K, V> entry : buckets[hash].getEntries()) {
      if (entry.getKey().equals(key)) {
        return entry;
      }
    }
    return null;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size() == 0;
  }

  @Override
  public boolean containsKey(Object key) {
    int hash = getHash(key);

    return !Objects.isNull(buckets[hash]) || !Objects.isNull(getEntry(key));
  }

  @Override
  public boolean containsValue(Object value) {

    for (HillelKeyValueBucket<K, V> bucket : buckets) {
      for (HilleKeyValueEntry<K, V> entry : bucket.getEntries()) {
        if (entry.getValue().equals(value)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public V get(Object key) {
    if (containsKey(key)) {
      return Objects.requireNonNull(getEntry(key)).getValue();
    }
    return null;
  }

  @Override
  public V put(K key, V value) {
    if (containsKey(key)) {
      Objects.requireNonNull(getEntry(key)).setValue(value);
    } else {
      int hash = getHash(key);
      if (Objects.isNull(buckets[hash])) {
        buckets[hash] = new HillelKeyValueBucket<>();
      }
      buckets[hash].addEntry(new HilleKeyValueEntry<>(key, value));
      size++;
    }
    return value;
  }

  @Override
  public V remove(Object key) {
    V value = null;
    if (containsKey(key)) {
      int hash = getHash(key);
      HilleKeyValueEntry<K, V> entry = getEntry(key);
      value = entry.getValue();
      buckets[hash].removeEntry(entry);
      size--;
    }
    return value;
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m) {
    for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
      put(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public void clear() {
    buckets = new HillelKeyValueBucket[CAPACITY];
    size = 0;
  }

  @Override
  public Set<K> keySet() {
    Set<K> keys = new HashSet<>();

    for (Entry<K, V> entry : entrySet()) {
      keys.add(entry.getKey());
    }

    return keys;
  }

  @Override
  public Collection<V> values() {
    Collection<V> values = new ArrayList<>();

    for (Entry<K, V> entry : entrySet()) {
      values.add(entry.getValue());
    }

    return values;
  }

  @Override
  public Set<Entry<K, V>> entrySet() {

    Set<Entry<K, V>> set = new HashSet<>();
    for (HillelKeyValueBucket<K, V> bucket : buckets) {
      set.addAll(bucket.getEntries());
    }
    return set;
  }
}
