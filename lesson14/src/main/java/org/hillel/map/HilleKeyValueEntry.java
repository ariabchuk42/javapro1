package org.hillel.map;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class HilleKeyValueEntry<K, V> implements Map.Entry<K, V> {

  @Setter
  private K key;
  private V value;

  @Override
  public V setValue(V value) {
    return value;
  }
}
