package org.hillel.map;

import java.util.LinkedList;
import java.util.List;

public class HillelKeyValueBucket<K, V> {

  private final List<HilleKeyValueEntry<K, V>> entries = new LinkedList<>();

  public List<HilleKeyValueEntry<K, V>> getEntries() {
    return entries;
  }

  public boolean addEntry(HilleKeyValueEntry<K, V> entry) {
    return entries.add(entry);
  }

  public boolean removeEntry(HilleKeyValueEntry<K, V> entry) {
    return entries.remove(entry);
  }
}
