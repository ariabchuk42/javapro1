package org.hillel.config.impl;

import java.util.Properties;
import org.hillel.Constants;
import org.hillel.logger.config.impl.SdtoutLoggerConfigurationLoader;
import org.hillel.logger.config.impl.StdoutLoggerConfiguration;
import org.hillel.logger.enums.LoggingLevel;
import org.hillel.services.PropertyReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StdoutLoggerConfigurationLoaderTest {

  private final static String LEVEL = "level";
  PropertyReader propertyReader = new PropertyReader();

  @Test
  void testFileLoggerConfigurationLoaderDefault() {

    StdoutLoggerConfiguration configuration = new SdtoutLoggerConfigurationLoader().load();
    Assertions.assertEquals(LoggingLevel.DEBUG, configuration.getLevel());
  }

  @Test
  void testFileLoggerConfigurationLoader() {

    Properties properties = propertyReader.getProperties(Constants.LOGGER_INFO_PROPERTIES);

    StdoutLoggerConfiguration configuration =
        new SdtoutLoggerConfigurationLoader().load(Constants.LOGGER_INFO_PROPERTIES);
    Assertions.assertEquals(properties.getProperty(LEVEL),
        configuration.getLevel().name().toLowerCase());
  }
}
