package org.hillel.service;

import org.hillel.logger.service.LoggerFactory;
import org.hillel.logger.service.impl.FileLogger;
import org.hillel.logger.service.impl.StdoutLogger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LoggerFactoryTest {
  @Test
  void testFactoryReturnsFileLogger() {

    Assertions.assertInstanceOf(FileLogger.class, new LoggerFactory().build(FileLogger.class));

  }

  @Test
  void testFactoryReturnsStdoutLogger() {

    Assertions.assertInstanceOf(StdoutLogger.class, new LoggerFactory().build(StdoutLogger.class));

  }

}
