package org.hillel.service.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.hillel.Constants;
import org.hillel.logger.config.impl.SdtoutLoggerConfigurationLoader;
import org.hillel.logger.service.Logger;
import org.hillel.logger.service.LoggerFactory;
import org.hillel.logger.service.impl.StdoutLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StdoutLoggerTest {

  ByteArrayOutputStream buffer;

  @BeforeEach
  void init() {
    buffer = new ByteArrayOutputStream();
    System.setOut(new PrintStream(buffer));

  }

  @Test
  void testLoggerLogInfoMessageLogLevelDebug()  {

    Logger logger = new LoggerFactory().build(StdoutLogger.class);
    logger.info(Constants.MESSAGE);

    String logs = buffer.toString();
    assertTrue(logs.contains(Constants.INFO));
    assertTrue(logs.contains(Constants.MESSAGE));
  }

  @Test
  void testLoggerLogDebugMessageLogLevelDebug() {

    Logger logger = new LoggerFactory().build(StdoutLogger.class);
    logger.debug(Constants.MESSAGE);

    String logs = buffer.toString();
    assertTrue(logs.contains(Constants.DEBUG));
    assertTrue(logs.contains(Constants.MESSAGE));
  }

  @Test
  void testLoggerLogInfoMessageLogLevelInfo() {

    Logger logger =
        new StdoutLogger(new SdtoutLoggerConfigurationLoader().load(Constants.LOGGER_INFO_PROPERTIES));

    logger.info(Constants.MESSAGE);

    String logs = buffer.toString();
    assertTrue(logs.contains(Constants.INFO));
    assertTrue(logs.contains(Constants.MESSAGE));
  }

  @Test
  void testLoggerLogDebugMessageLogLevelInfo() {

    Logger logger =
        new StdoutLogger(new SdtoutLoggerConfigurationLoader().load(Constants.LOGGER_INFO_PROPERTIES));
    logger.debug(Constants.MESSAGE);

    assertTrue(buffer.toString().isEmpty());
  }
}
