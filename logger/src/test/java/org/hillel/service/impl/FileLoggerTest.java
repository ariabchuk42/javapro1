package org.hillel.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.hillel.Constants;
import org.hillel.logger.config.impl.FileLoggerConfigurationLoader;
import org.hillel.logger.service.LoggerFactory;
import org.hillel.logger.service.impl.FileLogger;
import org.hillel.logger.service.Logger;
import org.hillel.services.PropertyReader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

public class FileLoggerTest {

  private final static String FILE = "file";
  private final static String FILTER = "log*.log";

  PropertyReader propertyReader = new PropertyReader();
  Properties properties = propertyReader.getProperties(Constants.LOGGER_PROPERTIES);
  String logDirectory = properties.get(FILE).toString();

  @Test
  void testLoggerCreateNewFiles() {

    Logger fileLogger = new LoggerFactory().build(FileLogger.class);

    for (int i = 0; i < 100; i++) {
      fileLogger.info(Constants.MESSAGE + i);
      fileLogger.debug(Constants.MESSAGE + i);
    }

    Collection<File> files =
        FileUtils.listFiles(new File(logDirectory), new WildcardFileFilter(FILTER),
            TrueFileFilter.TRUE);

    assertTrue(files.size() > 1);
  }

  @Test
  void testLoggerLogInfoMessageLogLevelDebug() throws IOException {

    Logger fileLogger = new LoggerFactory().build(FileLogger.class);
    fileLogger.info(Constants.MESSAGE);

    Collection<File> files =
        FileUtils.listFiles(new File(logDirectory), new WildcardFileFilter(FILTER),
            TrueFileFilter.TRUE);

    assertEquals(1, files.size());

    for (File file : files) {
      String logs = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
      assertTrue(logs.contains(Constants.INFO));
      assertTrue(logs.contains(Constants.MESSAGE));
    }
  }

  @Test
  void testLoggerLogDebugMessageLogLevelDebug() throws IOException {

    Logger fileLogger = new LoggerFactory().build(FileLogger.class);
    fileLogger.debug(Constants.MESSAGE);

    Collection<File> files =
        FileUtils.listFiles(new File(logDirectory), new WildcardFileFilter(FILTER),
            TrueFileFilter.TRUE);

    assertEquals(1, files.size());

    for (File file : files) {
      String logs = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
      assertTrue(logs.contains(Constants.DEBUG));
      assertTrue(logs.contains(Constants.MESSAGE));
    }
  }


  @Test
  void testLoggerLogInfoMessageLogLevelInfo() throws IOException {

    Logger fileLogger =
        new FileLogger(new FileLoggerConfigurationLoader().load(Constants.LOGGER_INFO_PROPERTIES));

    fileLogger.info(Constants.MESSAGE);

    Collection<File> files =
        FileUtils.listFiles(new File(logDirectory), new WildcardFileFilter(FILTER),
            TrueFileFilter.TRUE);

    assertEquals(1, files.size());

    for (File file : files) {
      String logs = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
      assertTrue(logs.contains(Constants.INFO));
      assertTrue(logs.contains(Constants.MESSAGE));
    }
  }

  @Test
  void testLoggerLogDebugMessageLogLevelInfo() {

    Logger fileLogger =
        new FileLogger(new FileLoggerConfigurationLoader().load(Constants.LOGGER_INFO_PROPERTIES));

    fileLogger.debug(Constants.MESSAGE);

    Collection<File> files =
        FileUtils.listFiles(new File(logDirectory), new WildcardFileFilter(FILTER),
            TrueFileFilter.TRUE);

    assertTrue(files.isEmpty());
  }

  @AfterEach
  void teardown() throws IOException {

    Path path = Paths.get(logDirectory);
    if (Files.exists(path)) {
      FileUtils.forceDelete(new File(logDirectory));
    }
  }
}
