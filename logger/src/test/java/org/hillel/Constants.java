package org.hillel;

public class Constants {

  public final static String LOGGER_PROPERTIES = "logger.properties";
  public final static String LOGGER_INFO_PROPERTIES = "loggerInfo.properties";
  public final static String MESSAGE = "test message";
  public final static String INFO = "INFO";
  public final static String DEBUG = "DEBUG";

}
