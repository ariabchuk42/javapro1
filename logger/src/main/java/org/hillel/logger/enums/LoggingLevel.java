package org.hillel.logger.enums;

public enum LoggingLevel {
  INFO,
  DEBUG
}
