package org.hillel.logger.exception;

public class FileMaxSizeReachedException extends RuntimeException {
  public FileMaxSizeReachedException(String message) {
    super(message);
  }
}
