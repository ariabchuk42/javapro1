package org.hillel.logger.service;

import org.hillel.logger.enums.LoggingLevel;

public interface Logger {

  void info(String message);
  void debug(String message);

}
