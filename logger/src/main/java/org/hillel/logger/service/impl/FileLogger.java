package org.hillel.logger.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.hillel.logger.config.impl.FileLoggerConfiguration;
import org.hillel.logger.config.impl.FileLoggerConfigurationLoader;
import org.hillel.logger.enums.LoggingLevel;
import org.hillel.logger.exception.FileMaxSizeReachedException;
import org.hillel.logger.service.Logger;
import org.hillel.logger.util.LoggerUtil;

@Setter
@AllArgsConstructor
public class FileLogger implements Logger {

  private final FileLoggerConfiguration configuration;

  private File file;

  @SneakyThrows
  public FileLogger() {
    FileLoggerConfigurationLoader loader = new FileLoggerConfigurationLoader();
    this.configuration = loader.load();
    Files.createDirectories(Paths.get(configuration.getPath()));
  }

  @SneakyThrows
  public FileLogger(FileLoggerConfiguration configuration) {
    this.configuration = configuration;
    Files.createDirectories(Paths.get(configuration.getPath()));
  }

  public void info(String message) {
    if (configuration.getLevel().ordinal() >= LoggingLevel.INFO.ordinal()) {
      logMessage(LoggerUtil.formatMessage(message, LoggingLevel.INFO));
    }
  }

  public void debug(String message) {
    if (configuration.getLevel().ordinal() >= LoggingLevel.DEBUG.ordinal()) {
      logMessage(LoggerUtil.formatMessage(message, LoggingLevel.DEBUG));
    }
  }

  @SneakyThrows
  private void logMessage(String message) {
    if (Objects.isNull(file)) {
      file = new File(getLogName());
    }
    try {
      writeToFile(message, file);
    } catch (FileMaxSizeReachedException e) {
      file = new File(getLogName());
      writeToFile(message, file);
    }
  }

  private void writeToFile(String message, File file) throws IOException {

    try (FileWriter writer = new FileWriter(file, true)) {
      if (Files.size(Paths.get(file.getAbsolutePath())) > this.configuration.getMaxSize()) {
        throw new FileMaxSizeReachedException("File reached size limits");
      }
      writer.write(message);
      writer.write("\n");
    }
  }



  private String getLogName() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm_ss.SSS");

    return new StringBuilder()
        .append(this.configuration.getPath())
        .append("log_")
        .append(formatter.format(LocalDateTime.now()))
        .append(this.configuration.getFormat()).toString();
  }
}
