package org.hillel.logger.service;

import lombok.SneakyThrows;

public class LoggerFactory {

  @SneakyThrows
  public <L extends Logger> L build(Class<L> clazz) {

    return clazz.newInstance();
  }
}
