package org.hillel.logger.service.impl;

import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.hillel.logger.config.LoggerConfiguration;
import org.hillel.logger.config.impl.SdtoutLoggerConfigurationLoader;
import org.hillel.logger.config.impl.StdoutLoggerConfiguration;
import org.hillel.logger.enums.LoggingLevel;
import org.hillel.logger.service.Logger;
import org.hillel.logger.util.LoggerUtil;

@Setter
@AllArgsConstructor
public class StdoutLogger implements Logger {

  private final StdoutLoggerConfiguration configuration;

  public StdoutLogger() {
    this.configuration =  new SdtoutLoggerConfigurationLoader().load();
  }

  public StdoutLogger(LoggerConfiguration configuration) {
    this.configuration = (StdoutLoggerConfiguration) configuration;
  }

  public void info(String message) {
    if (configuration.getLevel().ordinal() >= LoggingLevel.INFO.ordinal()) {
      logMessage(LoggerUtil.formatMessage(message, LoggingLevel.INFO));
    }
  }

  public void debug(String message) {
    if (configuration.getLevel().ordinal() >= LoggingLevel.DEBUG.ordinal()) {
      logMessage(LoggerUtil.formatMessage(message, LoggingLevel.DEBUG));
    }
  }

  @SneakyThrows
  private void logMessage(String message) {
    System.out.println(message);
  }
}
