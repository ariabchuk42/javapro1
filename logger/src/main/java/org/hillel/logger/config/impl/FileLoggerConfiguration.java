package org.hillel.logger.config.impl;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.hillel.logger.config.LoggerConfiguration;
import org.hillel.logger.enums.LoggingLevel;

@Getter
@SuperBuilder
public class FileLoggerConfiguration extends LoggerConfiguration {
  int maxSize;
  String path;
  String format;
}
