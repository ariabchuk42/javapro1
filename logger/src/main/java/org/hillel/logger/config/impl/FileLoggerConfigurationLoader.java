package org.hillel.logger.config.impl;

import java.util.Properties;
import lombok.AllArgsConstructor;
import org.hillel.logger.config.LoggerConfigurationLoader;
import org.hillel.logger.config.impl.FileLoggerConfiguration;
import org.hillel.logger.enums.LoggingLevel;
import org.hillel.services.PropertyReader;

@AllArgsConstructor
public class FileLoggerConfigurationLoader implements LoggerConfigurationLoader {

  PropertyReader reader;

  public FileLoggerConfigurationLoader() {
    reader = new PropertyReader();
  }

  public FileLoggerConfiguration load() {
    return load("logger.properties");
  }

  public FileLoggerConfiguration load(String path) {

    Properties properties = reader.getProperties(path);

    return FileLoggerConfiguration.builder()
        .maxSize(Integer.parseInt(properties.getProperty("max-size")))
        .format(properties.getProperty("format"))
        .level(LoggingLevel.valueOf(properties.getProperty("level").toUpperCase()))
        .path(properties.getProperty("file"))
        .build();
  }
}
