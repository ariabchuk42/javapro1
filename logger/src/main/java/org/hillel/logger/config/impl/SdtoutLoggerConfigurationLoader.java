package org.hillel.logger.config.impl;

import java.util.Properties;
import lombok.AllArgsConstructor;
import org.hillel.logger.config.LoggerConfigurationLoader;
import org.hillel.logger.enums.LoggingLevel;
import org.hillel.services.PropertyReader;

@AllArgsConstructor
public class SdtoutLoggerConfigurationLoader implements LoggerConfigurationLoader {

  PropertyReader reader;

  public SdtoutLoggerConfigurationLoader() {
    reader = new PropertyReader();
  }

  public StdoutLoggerConfiguration load(String path) {

    Properties properties = reader.getProperties(path);

    return StdoutLoggerConfiguration.builder()
        .level(LoggingLevel.valueOf(properties.getProperty("level").toUpperCase()))
        .build();
  }

  public StdoutLoggerConfiguration load() {
    return StdoutLoggerConfiguration.builder()
        .level(LoggingLevel.DEBUG)
        .build();
  }

}
