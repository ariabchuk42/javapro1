package org.hillel.logger.config.impl;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.hillel.logger.config.LoggerConfiguration;

@Getter
@SuperBuilder
public class StdoutLoggerConfiguration extends LoggerConfiguration {
}
