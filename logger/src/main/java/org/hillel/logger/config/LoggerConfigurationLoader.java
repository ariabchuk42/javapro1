package org.hillel.logger.config;

public interface LoggerConfigurationLoader {

  LoggerConfiguration load();

}
