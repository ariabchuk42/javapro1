package org.hillel.logger.config;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.hillel.logger.enums.LoggingLevel;

@Getter
@SuperBuilder
public abstract class LoggerConfiguration {

  private LoggingLevel level;

}
