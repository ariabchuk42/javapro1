package org.hillel;

import org.hillel.entity.Hotel;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
  public static void main(String[] args) {

    ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:ApplicationConfig.xml");

    Hotel hotel = ctx.getBean(Hotel.class);

    System.out.println(hotel);
    hotel.setName("Spring Hotel");

    Hotel hotel2 = ctx.getBean(Hotel.class);

    System.out.println(hotel2);

  }
}