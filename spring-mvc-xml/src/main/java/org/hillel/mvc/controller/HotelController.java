package org.hillel.mvc.controller;

import lombok.RequiredArgsConstructor;
import org.hillel.mvc.entity.Hotel;
import org.hillel.mvc.repository.HotelRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/hotels")
public class HotelController {

  private final HotelRepository repository;

  @GetMapping()
  public String hotels(Model model) {

    model.addAttribute("hotels", repository.getAll());

    return "hotels/all";

  }

  @GetMapping("/{id}")
  public String hotel(@PathVariable("id") Long id, Model model) {

    model.addAttribute("hotel", repository.getById(id));

    return "hotels/byId";

  }

  @GetMapping("/new")
  public String newHotel(@ModelAttribute("hotel") Hotel hotel) {

    return "hotels/new";
  }

  @PostMapping
  public String add(@ModelAttribute("hotel") Hotel hotel) {

    repository.save(hotel);

    return "redirect:/hotels";
  }


}
