package org.hillel.mvc.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hotel {

  private Long id;
  private String name;
  private String description;
  private String lang;
  private int ranking;
}