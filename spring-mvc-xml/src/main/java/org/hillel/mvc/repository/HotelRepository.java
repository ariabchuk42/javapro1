package org.hillel.mvc.repository;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.hillel.mvc.entity.Hotel;
import org.springframework.stereotype.Repository;

@Repository
public class HotelRepository {

  private static Long ID = 0L;
  private final List<Hotel> hotels;

  public HotelRepository() {

    hotels = new ArrayList<>();

    Hotel hotel1 = Hotel.builder()
        .id(++ID)
        .name("Hotel 1")
        .description("Hotel MVC 1")
        .lang("ENG")
        .ranking(1)
        .build();

    Hotel hotel2 = Hotel.builder()
        .id(++ID)
        .name("Hotel 2")
        .description("Hotel MVC 2")
        .lang("ENG")
        .ranking(2)
        .build();

    Hotel hotel3 = Hotel.builder()
        .id(++ID)
        .name("Hotel 3")
        .description("Hotel MVC 3")
        .lang("ENG")
        .ranking(3)
        .build();

    hotels.add(hotel1);
    hotels.add(hotel2);
    hotels.add(hotel3);
  }

  public List<Hotel> getAll() {
    return hotels;
  }

  public Hotel getById(Long id) {
    return hotels.stream()
        .filter(h -> h.getId().equals(id))
        .findAny()
        .orElse(null);
  }

  public void save(Hotel hotel) {
    hotel.setId(++ID);
    hotels.add(hotel);
  }

}
