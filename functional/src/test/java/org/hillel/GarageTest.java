package org.hillel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

public class GarageTest {

  @Test
  void testParking() {

    Garage garage = new Garage();
    garage.park(new Toyota());

    garage.park((s) -> {
      System.out.println("Im driving Nissan " + s +" side");
      System.out.println("hahaha");
    });


    System.out.println("-------------------------");

    for (Car car : garage.getCars()) {
      car.drive("left");
    }
  }

  @Test
  void testMethodRef() {
    Garage garage = new Garage();
    garage.park(GarageTest::carMethod);

    garage.park(this::carMethodNonStatic);
    garage.park(Garage::getStat);
    garage.park(garage::getNonStat);

  }

  public static void carMethod(String sss) {
    return;
  }

  public void carMethodNonStatic(String sss) {
    return;
  }

  @Test
  void testSupplier() throws InterruptedException {

    Supplier<LocalDateTime> s = () -> LocalDateTime.now();

    System.out.println("Current date time:");
    System.out.println(LocalDateTime.now());

    Thread.sleep(22000);

    System.out.println();

    print(s);

  }

  private void print(Supplier<LocalDateTime> val) {
    System.out.println("Supplier date time:");
    System.out.println(val.get());
  }

  @Test
  void testConsumer() {

    Consumer<Integer> c = v -> System.out.println(v);

    c.accept(25);

  }

  @Test
  void testFunction() {

    Function<Integer, String> f = i -> "my favorite number is " + i.toString();

    System.out.println(f.apply(36));

  }

  @Test
  void testBiFunctions() {

    BiFunction<Integer, String, Pair> b = (i, s) -> Pair.builder()
        .key(i)
        .value(s)
        .build();

    System.out.println(b.apply(23, "lesson"));

  }

  @Getter
  @Setter
  @Builder
  @ToString
  public static class Pair {
    Integer key;
    String value;

    public static String getString() {
      return "method";
    }
  }

  @Test
  void testOptional() {

    Pair pair = Pair.builder()
        .key(22)
        .value("hillel")
        .build();

    Optional<Pair> optionalPair = Optional.ofNullable(pair);

    optionalPair.ifPresent(p -> System.out.println(p));

    Optional<String> stringOptional = optionalPair
        .map(p -> p.getValue());

    Optional<Pair> optional = optionalPair.filter(p -> p.key.equals(23));

    Assertions.assertEquals(Optional.empty(), optional);


  }

  @Test
  void testOptional2() {

    Pair pair = null;

    Optional<Pair> optionalPair = Optional.ofNullable(pair);

    Pair pair1 = optionalPair.orElse(Pair.builder().key(27).build());
    System.out.println(pair1);


  }

  @Test
  void testOptional3() {

    Pair pair = null;

    Optional<Pair> optionalPair = Optional.ofNullable(pair);

    Pair pair1 = optionalPair.orElseGet(() -> Pair.builder().value("mavpa").build());
    System.out.println(pair1);

  }

//  @Test
//  void testOptional4() {
//
//    Pair pair = null;
//
//    Optional<Pair> optionalPair = Optional.ofNullable(pair);
//
//    Pair pair1 = optionalPair.orElseThrow(() -> new RuntimeException("no such pair"));
//    System.out.println(pair1);
//
//  }

  @Test
  void testOptional5() {

    Toyota toyota = new Toyota();

    Optional<Car> optionalPair = Optional.ofNullable(toyota);

    Car pair1 = optionalPair.orElseGet(() -> new Kia());
    System.out.println(pair1);

  }

//  @Test
//  void testOptional6() {
//
//    Toyota toyota = new Toyota();
//
//    Optional<Car> optionalPair = Optional.ofNullable(toyota);
//
//    Car pair1 = optionalPair.orElse(new Kia());
//    System.out.println(pair1);
//
//  }

  @Test
  void testOptional7() {

    Pair pair = Pair.builder()
        .key(22)
        .value("hillel")
        .build();

    Toyota car = Optional.ofNullable(pair)
        .map(p -> new Toyota())
        .orElseGet(Toyota::new);
  }

  @Test
  void testOptional8() {

    Pair pair = Pair.builder()
        .key(22)
        .value("hillel")
        .build();

    String val = Optional.ofNullable(pair)
        .map(p -> Pair.getString()).orElse("");
  }

  private String getString() {
    return "method";
  }
}
