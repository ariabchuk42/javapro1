package org.hillel;

@FunctionalInterface
public interface Car {

  default void stop() {
    System.out.println("Car stopped");
  }
  void drive(String side);
}
