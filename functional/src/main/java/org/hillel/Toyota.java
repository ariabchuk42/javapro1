package org.hillel;

import lombok.ToString;

@ToString
public class Toyota implements Car {
  @Override
  public void drive(String side) {
    System.out.println("Im driving Toyota on " + side + " side");
  }
}
