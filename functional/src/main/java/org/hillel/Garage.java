package org.hillel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class Garage {

  List<Car> cars = new ArrayList<>();

  public void park(Car car) {
    car.drive("right");
    cars.add(car);

  }

  public void getNonStat(String hh) {

  }

  public static void getStat(String ddd) {

  }
}
