package org.hillel.java.pro;

import java.io.IOException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TextProviderTest {

  @Test
  void testText() throws IOException {

    Assertions.assertEquals("fox", TextProvider.getText());
  }

}
