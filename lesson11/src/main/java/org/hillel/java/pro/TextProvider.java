package org.hillel.java.pro;

import java.io.IOException;
import org.hillel.services.PropertyReader;

public class TextProvider {

  private static final PropertyReader propertyReader = new PropertyReader();

  public static String getText() throws IOException {

    return propertyReader.getProperties("text.properties").getProperty("kolobok", "fox");
  }
}
