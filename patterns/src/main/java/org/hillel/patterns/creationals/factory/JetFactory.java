package org.hillel.patterns.creationals.factory;

public class JetFactory implements Factory<Jet> {

  public Jet build(String jet) {

    switch (jet) {
      case ("Biong"): return new Boing();
      case ("AirBus"): return new AirBus();
      default: return new Jet();
    }
  }
}
