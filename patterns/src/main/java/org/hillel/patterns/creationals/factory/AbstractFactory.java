package org.hillel.patterns.creationals.factory;

public class AbstractFactory  {

  public Factory getFactory(String type) {

    switch (type) {
      case ("car"): return new CarFactory();
      case ("jet"): return new JetFactory();
      default: return null;
    }
  }

}
