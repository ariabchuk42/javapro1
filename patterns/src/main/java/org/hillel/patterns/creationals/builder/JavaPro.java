package org.hillel.patterns.creationals.builder;


public class JavaPro {

  private String name;
  private int lessonsAmount;


  public static Builder builder() {
    return new Builder();
  }


  public static class Builder {
    private String name;
    private int lessonsAmount;



    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder lessonsAmount(int lessonsAmount) {
      this.lessonsAmount = lessonsAmount;
      return this;
    }

    public JavaPro build() {

      JavaPro javaPro = new JavaPro();
      javaPro.name = this.name;
      javaPro.lessonsAmount = this.lessonsAmount;
      return javaPro;
    }
  }

}
