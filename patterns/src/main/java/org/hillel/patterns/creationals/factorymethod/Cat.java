package org.hillel.patterns.creationals.factorymethod;


import lombok.Getter;

@Getter
public class Cat {

  private String name;

  private Cat(String name) {
    this.name = name;
  }

  public static Cat getCat(String name) {

    return new Cat(name);
  }

}
