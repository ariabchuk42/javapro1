package org.hillel.patterns.creationals.factory;

public class Car {

  public void drive() {
    System.out.println("Car is driving");
  }

  public <V> void print(V val) {

    System.out.println("V has: " + val);
  }

}
