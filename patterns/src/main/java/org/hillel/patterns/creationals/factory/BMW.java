package org.hillel.patterns.creationals.factory;

public class BMW extends Car {

  @Override
  public void drive() {
    System.out.println("BMW is driving");
  }
}
