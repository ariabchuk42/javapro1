package org.hillel.patterns.creationals.factory;

public interface Factory<T> {

  T build(String type);

}
