package org.hillel.patterns.creationals.singleton;

import java.time.Instant;

public enum Singleton {

  INSTANCE;

  private String name;
  private Instant time;

  public Instant getTime() {
    return time;
  }

  public Instant setTime() {
    return time = Instant.now();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
