package org.hillel.patterns.creationals.factory;

public class X5 extends BMW {

  @Override
  public void drive() {
    System.out.println("X5 is driving");
  }
}
