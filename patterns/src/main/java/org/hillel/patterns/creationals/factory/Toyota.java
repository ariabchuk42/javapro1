package org.hillel.patterns.creationals.factory;

public class Toyota extends Car {

  @Override
  public void drive() {
    System.out.println("Toyota is driving");
  }
}
