package org.hillel.patterns.creationals.lazy;

import java.util.Objects;

public class LazyClass {

  private String name;

  public String getName() {
    if (Objects.isNull(name)) {
      name = "Lazy";
    }
    return name;
  }

}
