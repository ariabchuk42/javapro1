package org.hillel.patterns.creationals.factory;

public class CarFactory implements Factory<Car> {

  public Car build(String car) {

    switch (car) {
      case ("BMW"): return new BMW();
      case ("Toyota"): return new Toyota();
      default: return new Car();
    }
  }
}
