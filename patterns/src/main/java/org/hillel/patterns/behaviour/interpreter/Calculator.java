package org.hillel.patterns.behaviour.interpreter;

import java.util.LinkedList;

public class Calculator implements Expression {

  Expression calc;

  public Calculator(String expression) {

    LinkedList<Expression> expressions = new LinkedList<>();

    for (String exp : expression.split("\\D")) {
      expressions.add(new Number(Integer.parseInt(exp)));
    }
    for (String exp : expression.split("\\d")) {
      if (exp.equals("+")) {
        expressions.add(new Plus(expressions.poll(), expressions.poll()));
      } else if (exp.equals("-")) {
        expressions.add(new Minus(expressions.poll(), expressions.poll()));
      }
    }
    calc = expressions.poll();
  }

  @Override
  public int interpret(Expression expression) {
    return calc.interpret(expression);
  }
}
