package org.hillel.patterns.behaviour.interpreter;

public class Plus implements Expression {

  private Expression right;
  private Expression left;

  public Plus(Expression left, Expression right) {
    this.right = right;
    this.left = left;
  }

  @Override
  public int interpret(Expression expression) {
    return right.interpret(expression) + left.interpret(expression);
  }
}
