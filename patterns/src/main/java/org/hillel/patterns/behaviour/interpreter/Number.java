package org.hillel.patterns.behaviour.interpreter;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Number implements Expression {
  private int number;

  @Override
  public int interpret(Expression expression) {
    return number;
  }
}
