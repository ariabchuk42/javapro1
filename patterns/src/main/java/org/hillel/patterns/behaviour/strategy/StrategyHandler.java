package org.hillel.patterns.behaviour.strategy;

public enum StrategyHandler {

  INSTANCE;
  private Strategy strategy;

  public void setStrategy(Strategy strategy) {
    this.strategy = strategy;
  }

  public Strategy getStrategy() {
    return strategy;
  }
}
