package org.hillel.patterns.behaviour.interpreter;

public interface Expression {
  int interpret(Expression expression);
}
