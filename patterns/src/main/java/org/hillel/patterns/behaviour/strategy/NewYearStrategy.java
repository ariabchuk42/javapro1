package org.hillel.patterns.behaviour.strategy;

public class NewYearStrategy implements Strategy {
  @Override
  public double getPrice(double price) {
    return price * 0.4;
  }
}
