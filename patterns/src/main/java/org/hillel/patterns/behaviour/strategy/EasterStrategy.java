package org.hillel.patterns.behaviour.strategy;

public class EasterStrategy implements Strategy {
  @Override
  public double getPrice(double price) {
    return price * 0.7;
  }
}
