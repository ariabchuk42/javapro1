package org.hillel.patterns.behaviour.strategy;

public interface Strategy {

  double getPrice(double price);
}
