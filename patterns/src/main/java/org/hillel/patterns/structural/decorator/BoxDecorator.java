package org.hillel.patterns.structural.decorator;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BoxDecorator implements Box {

  private Box box;

  @Override
  public String add() {
    return box.add();
  }
}
