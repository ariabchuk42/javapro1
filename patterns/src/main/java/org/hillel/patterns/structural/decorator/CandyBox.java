package org.hillel.patterns.structural.decorator;

public class CandyBox extends BoxDecorator {
  public CandyBox(Box box) {
    super(box);
  }

  @Override
  public String add() {
    return super.add() + withCandies();
  }

  private String withCandies() {
    return " with candies";
  }
}
