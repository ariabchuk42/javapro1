package org.hillel.patterns.structural.bridge.bad;

public class BlueCircle extends Circle {

  @Override
  public String draw() {
    return super.draw() + " Color is Blue";
  }
}
