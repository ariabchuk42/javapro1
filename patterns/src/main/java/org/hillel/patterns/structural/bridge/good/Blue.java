package org.hillel.patterns.structural.bridge.good;

public class Blue implements Color {
  @Override
  public String fill() {
    return "Color is Blue";
  }
}
