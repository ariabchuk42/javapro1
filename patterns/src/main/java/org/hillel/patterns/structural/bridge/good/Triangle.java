package org.hillel.patterns.structural.bridge.good;

public class Triangle extends Shape {

  public Triangle(Color color) {
    this.color = color;
  }

  @Override
  public String draw() {
    return "Triangle drawn." + getColor().fill();
  }
}
