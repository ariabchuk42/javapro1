package org.hillel.patterns.structural.bridge.bad;

public class BlueTriangle extends Triangle {

  @Override
  public String draw() {
    return super.draw() + " Color is Blue";
  }
}
