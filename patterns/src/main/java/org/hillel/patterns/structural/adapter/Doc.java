package org.hillel.patterns.structural.adapter;

public class Doc {

  public void healPerson(String name) {
    System.out.println("Im a doctor, Im healing " + name);
  }
}
