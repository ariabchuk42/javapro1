package org.hillel.patterns.structural.bridge.good;

import lombok.Getter;

@Getter
public abstract class Shape {

  protected Color color;

  public abstract String draw();
}
