package org.hillel.patterns.structural.bridge.bad;

public class Triangle implements Shape {
  @Override
  public String draw() {
    return "Triangle drawn.";
  }
}
