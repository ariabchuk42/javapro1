package org.hillel.patterns.structural.bridge.bad;

public class RedTriangle extends Triangle {

  @Override
  public String draw() {
    return super.draw() + " Color is Red";
  }
}
