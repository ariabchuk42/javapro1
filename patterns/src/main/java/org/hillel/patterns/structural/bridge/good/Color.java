package org.hillel.patterns.structural.bridge.good;

public interface Color {

  String fill();

}
