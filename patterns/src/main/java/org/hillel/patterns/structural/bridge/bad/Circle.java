package org.hillel.patterns.structural.bridge.bad;

public class Circle implements Shape {
  @Override
  public String draw() {
    return "Circle drawn.";
  }
}
