package org.hillel.patterns.structural.bridge.good;

public class Red implements Color {
  @Override
  public String fill() {
    return "Color is Red";
  }
}
