package org.hillel.patterns.structural.decorator;

public class CookiesBox extends BoxDecorator {
  public CookiesBox(Box box) {
    super(box);
  }

  @Override
  public String add() {
    return super.add() + withCookies();
  }

  private String withCookies() {
    return " with cookies";
  }
}
