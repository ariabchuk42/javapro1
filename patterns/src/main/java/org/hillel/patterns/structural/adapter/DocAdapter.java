package org.hillel.patterns.structural.adapter;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DocAdapter extends Doc {

  private ZooDoc zooDoc;

  public void healPerson(String name) {
    zooDoc.healAnimal(name);
  }

}
