package org.hillel.patterns.structural.decorator;

public class ToyBox extends BoxDecorator {
  public ToyBox(Box box) {
    super(box);
  }

  @Override
  public String add() {
    return super.add() + withToys();
  }

  private String withToys() {
    return " with toys";
  }
}
