package org.hillel.patterns.structural.bridge.good;

public class Circle extends Shape {

  public Circle(Color color) {
    this.color = color;
  }
  @Override
  public String draw() {
    return "Circle drawn." + getColor().fill();
  }
}
