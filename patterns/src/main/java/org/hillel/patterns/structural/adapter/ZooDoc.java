package org.hillel.patterns.structural.adapter;

public class ZooDoc {

  public void healAnimal(String name) {
    System.out.println("Im animal doc, Im healing animal " + name);
  }
}
