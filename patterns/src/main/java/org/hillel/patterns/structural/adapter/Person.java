package org.hillel.patterns.structural.adapter;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Person {

  private String name;

  public void orderDoctor(Doc doc) {
    doc.healPerson(name);
  }


}
