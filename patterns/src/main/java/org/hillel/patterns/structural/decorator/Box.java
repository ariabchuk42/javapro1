package org.hillel.patterns.structural.decorator;

public interface Box {

  String add();

}
