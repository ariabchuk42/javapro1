package org.hillel.patterns.structural.bridge.bad;

public interface Shape {
  String draw();
}
