package org.hillel.patterns.structural.decorator;

public class BoxImp implements Box {
  @Override
  public String add() {
    return "Our Box";
  }
}
