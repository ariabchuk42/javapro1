package org.hillel.patterns.structural.bridge.bad;

public class RedCircle extends Circle {

  @Override
  public String draw() {
    return super.draw() + " Color is Red";
  }
}
