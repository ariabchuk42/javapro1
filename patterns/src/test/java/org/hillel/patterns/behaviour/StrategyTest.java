package org.hillel.patterns.behaviour;

import org.hillel.patterns.behaviour.strategy.HalfPriceStrategy;
import org.hillel.patterns.behaviour.strategy.StrategyHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class StrategyTest {

  @BeforeAll
  static void init() {
    StrategyHandler.INSTANCE.setStrategy(new HalfPriceStrategy());
  }

  @Test
  void test() {

    double price = StrategyHandler.INSTANCE.getStrategy().getPrice(200);
    System.out.println(price);

    Assertions.assertEquals(100, price);

  }
}
