package org.hillel.patterns.behaviour;


import org.hillel.patterns.behaviour.interpreter.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalcTest {

  @Test
  void test() {

    String expression = "2+5-3";

    Calculator calculator = new Calculator(expression);
    int result = calculator.interpret(calculator);

    Assertions.assertEquals(4, result);

  }
}
