package org.hillel.patterns.creationals;

import org.hillel.patterns.creationals.builder.JavaPro;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BuilderTest {

  @Test
  void test() {

    JavaPro build = JavaPro.builder()
        .name("31012023")
        .build();

    Assertions.assertInstanceOf(JavaPro.class, build);

  }
}
