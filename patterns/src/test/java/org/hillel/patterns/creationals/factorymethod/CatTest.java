package org.hillel.patterns.creationals.factorymethod;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CatTest {

  @Test
  void test() {

    Assertions.assertEquals("cat", Cat.getCat("cat").getName());
  }

}
