package org.hillel.patterns.creationals.factory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CarFactoryTest {


  @Test
  void testCarFactory() {

    CarFactory factory = new CarFactory();
    Assertions.assertInstanceOf(BMW.class, factory.build("BMW"));
    Assertions.assertInstanceOf(Toyota.class, factory.build("Toyota"));
    Assertions.assertInstanceOf(Car.class, factory.build("ZAZ"));

  }
}
