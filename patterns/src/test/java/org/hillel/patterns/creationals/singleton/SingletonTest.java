package org.hillel.patterns.creationals.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;


public class SingletonTest {

  @Test
  @Order(1)
  void test() {

    Singleton.INSTANCE.setTime();
    Singleton.INSTANCE.setName("Varvara");

  }

  @Test
  @Order(2)
  void test2() {

    Assertions.assertEquals("Varvara", Singleton.INSTANCE.getName());

  }


}
