package org.hillel.patterns.creationals.factory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AbstractFactoryTest {

  @Test
  void test() {


    AbstractFactory abstractFactory = new AbstractFactory();

    Assertions.assertInstanceOf(BMW.class, abstractFactory.getFactory("car").build("BMW"));
    Assertions.assertInstanceOf(AirBus.class, abstractFactory.getFactory("jet").build("AirBus"));
  }
}
