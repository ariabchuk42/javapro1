package org.hillel.patterns.structural.bridge;

import org.hillel.patterns.structural.bridge.bad.BlueCircle;
import org.hillel.patterns.structural.bridge.bad.BlueTriangle;
import org.hillel.patterns.structural.bridge.bad.RedCircle;
import org.hillel.patterns.structural.bridge.bad.RedTriangle;
import org.hillel.patterns.structural.bridge.good.Blue;
import org.hillel.patterns.structural.bridge.good.Circle;
import org.hillel.patterns.structural.bridge.good.Red;
import org.hillel.patterns.structural.bridge.good.Triangle;
import org.junit.jupiter.api.Test;

public class BridgeTest {

  @Test
  void testBad() {

    System.out.println(new BlueCircle().draw());
    System.out.println(new RedCircle().draw());
    System.out.println(new RedTriangle().draw());
    System.out.println(new BlueTriangle().draw());

  }

  @Test
  void testGood() {

    Blue blue = new Blue();
    Red red = new Red();

    System.out.println(new Circle(blue).draw());
    System.out.println(new Circle(red).draw());
    System.out.println(new Triangle(blue).draw());
    System.out.println(new Triangle(red).draw());

  }


}
