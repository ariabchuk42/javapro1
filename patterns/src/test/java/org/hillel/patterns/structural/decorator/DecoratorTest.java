package org.hillel.patterns.structural.decorator;

import org.junit.jupiter.api.Test;

public class DecoratorTest {

  @Test
  void testDecorator() {

    BoxImp box = new BoxImp();
    CandyBox candyBox = new CandyBox(box);
    CookiesBox cookiesBox = new CookiesBox(candyBox);
    ToyBox toyBox = new ToyBox(cookiesBox);

    System.out.println(toyBox.add());

  }

}
