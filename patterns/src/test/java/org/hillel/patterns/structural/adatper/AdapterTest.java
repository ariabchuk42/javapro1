package org.hillel.patterns.structural.adatper;

import org.hillel.patterns.structural.adapter.Doc;
import org.hillel.patterns.structural.adapter.DocAdapter;
import org.hillel.patterns.structural.adapter.Person;
import org.hillel.patterns.structural.adapter.ZooDoc;
import org.junit.jupiter.api.Test;

public class AdapterTest {

  @Test
  void test() {

    Person zoi = new Person("Zoi");
    //zoi.orderDoctor(new Doc());


    Doc doc2 = new DocAdapter(new ZooDoc());

    zoi.orderDoctor(doc2);

  }
}
