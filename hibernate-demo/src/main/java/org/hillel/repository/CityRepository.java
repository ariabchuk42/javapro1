package org.hillel.repository;

import org.hillel.entity.City;

public class CityRepository extends CrudRepository<City> {
  public CityRepository() {
    super(City.class);
  }
}
