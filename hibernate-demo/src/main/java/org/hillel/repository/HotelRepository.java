package org.hillel.repository;

import org.hillel.entity.Hotel;

public class HotelRepository extends CrudRepository<Hotel> {
  public HotelRepository() {
    super(Hotel.class);
  }
}
