package org.hillel.repository;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.query.criteria.JpaCriteriaQuery;
import org.hillel.config.SFactory;

public abstract class CrudRepository<T> {

  private final Class<T> entityClass;

  protected CrudRepository(Class<T> entityClass) {
    this.entityClass = entityClass;
  }

  public T getById(Long id) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      return session.get(entityClass, id);
    }
  }

  public List<T> getAll() {
    try (Session session = SFactory.INSTANCE.openSession()) {

      HibernateCriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
      JpaCriteriaQuery<T> query = criteriaBuilder.createQuery(entityClass);
      query.from(entityClass);
      return session.createQuery(query).getResultList();
    }
  }

  public void save(T entity) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      Transaction transaction = session.beginTransaction();
      session.persist(entity);
      transaction.commit();
    }
  }

  public void update(T entity) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      Transaction transaction = session.beginTransaction();
      session.update(entity);
      transaction.commit();
    }
  }

  public void delete(Long id) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      Transaction transaction = session.beginTransaction();
      session.delete(session.get(entityClass, id));
      transaction.commit();
    }
  }
}
