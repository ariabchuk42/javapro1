package org.hillel.repository;

import org.hillel.entity.Visitor;

public class VisitorRepository extends CrudRepository<Visitor> {
  public VisitorRepository() {
    super(Visitor.class);
  }
}
