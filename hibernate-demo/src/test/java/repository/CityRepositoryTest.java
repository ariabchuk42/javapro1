package repository;

import java.util.List;
import org.hillel.entity.City;
import org.hillel.repository.CityRepository;
import org.hillel.repository.CrudRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CityRepositoryTest {

  CrudRepository<City> repo = new CityRepository();

  @Test
  void getByIdTest() {

    City entity = repo.getById(2L);


    Assertions.assertEquals("Paris", entity.getName());

    System.out.println(entity);

  }

//  @Test
//  void saveHotelTest() {
//
//    Hotel hotel = Hotel.builder()
//        .name("hibernate hotel")
//        .description("hibernate hotel descr")
//        .lang("ENG")
//        .ranking(2)
//        .build();
//
//    repo.save(hotel);
//
//  }
//
//  @Test
//  void updateHotel() {
//
//    Hotel hotel = repo.getById(1L);
//    hotel.setName("new name");
//
//    repo.update(hotel);
//  }
//
//  @Test
//  void deleteHotel() {
//
//    repo.delete(21L);
//  }

  @Test
  void getAll() {

    List<City> all = repo.getAll();

    Assertions.assertTrue(all.size() > 1);
  }

}
