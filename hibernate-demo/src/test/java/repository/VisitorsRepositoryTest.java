package repository;

import java.util.List;
import org.hillel.entity.Visitor;
import org.hillel.repository.CrudRepository;
import org.hillel.repository.VisitorRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class VisitorsRepositoryTest {

  CrudRepository<Visitor> repo = new VisitorRepository();

  @Test
  void getByIdTest() {

    Visitor entity = repo.getById(3L);

    Assertions.assertEquals("Taras", entity.getFirstName());

  }

//  @Test
//  void saveHotelTest() {
//
//    Hotel hotel = Hotel.builder()
//        .name("hibernate hotel")
//        .description("hibernate hotel descr")
//        .lang("ENG")
//        .ranking(2)
//        .build();
//
//    repo.save(hotel);
//
//  }
//
//  @Test
//  void updateHotel() {
//
//    Hotel hotel = repo.getById(1L);
//    hotel.setName("new name");
//
//    repo.update(hotel);
//  }
//
//  @Test
//  void deleteHotel() {
//
//    repo.delete(21L);
//  }

  @Test
  void getAll() {

    List<Visitor> all = repo.getAll();

    Assertions.assertTrue(all.size() > 1);
  }

}
