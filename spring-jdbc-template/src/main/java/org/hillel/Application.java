package org.hillel;

import org.hillel.config.AppConfig;
import org.hillel.entity.Hotel;
import org.hillel.repository.HotelRepository;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
  public static void main(String[] args) {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

    HotelRepository hotelRepository = context.getBean(HotelRepository.class);

    //hotelRepository.getAll().forEach(System.out::println);
//    Hotel hotel = hotelRepository.getById(3L);
//
//    Hotel byNameAndDescription = hotelRepository.getByNameAndDescription("hilton4", "hilton hotel");
//
//    System.out.println(hotel);
//    System.out.println(byNameAndDescription);

//    Hotel hotel = Hotel.builder()
//        .name("spring template hotel")
//        .description("spring template hotel description")
//        .lang("ENG")
//        .ranking(4)
//        .build();
//
//    hotelRepository.create(hotel);

    //System.out.println(hotelRepository.delete(23L));
    //System.out.println(hotelRepository.deleteByName("Hotel Entity2"));

  }
}