package org.hillel.repository;

import java.util.List;
import lombok.Data;
import org.hillel.entity.Hotel;
import org.hillel.mapper.HotelRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Data
@Repository
public class HotelRepository {

  private final JdbcTemplate template;
  private final HotelRowMapper mapper;

  public List<String> getHotelNames() {

    String sql = "SELECT NAME FROM hotels";
    return template.queryForList(sql, String.class);

  }

  public List<Hotel> getAll() {

    String sql = "SELECT * FROM hotels";
    return template.query(sql, mapper);

  }

  public Hotel getById(Long id) {

    String sql = "SELECT * FROM hotels WHERE ID = ?";
    return template.queryForObject(sql, mapper, id);

  }

  public Hotel getByNameAndDescription(String name, String description) {

    String sql = "SELECT * FROM hotels WHERE NAME = ? AND DESCRIPTION = ?";
    return template.queryForObject(sql, mapper, name, description);

  }

  public int create(Hotel hotel) {

    String sql = "INSERT INTO hotels(name, description, lang, ranking) VALUES(?, ?, ?, ?)";
    return template.update(sql, hotel.getName(), hotel.getDescription(), hotel.getLang(), hotel.getRanking());

  }

  public int delete(Long id) {

    String sql = "DELETE FROM hotels WHERE ID = ?";
    return template.update(sql, id);

  }

  public int deleteByName(String name) {

    String sql = "DELETE FROM hotels WHERE NAME = ?";
    return template.update(sql, name);

  }
}
