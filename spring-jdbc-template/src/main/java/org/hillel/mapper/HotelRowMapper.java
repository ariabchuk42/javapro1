package org.hillel.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import lombok.RequiredArgsConstructor;
import org.hillel.entity.Hotel;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class HotelRowMapper implements RowMapper<Hotel> {

  private final HotelResultSetExtractor extractor;

  @Override
  public Hotel mapRow(ResultSet rs, int rowNum) throws SQLException {
    return extractor.extractData(rs);
  }
}
