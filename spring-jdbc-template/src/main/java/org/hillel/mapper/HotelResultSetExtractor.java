package org.hillel.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.hillel.entity.Hotel;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

@Component
public class HotelResultSetExtractor implements ResultSetExtractor<Hotel> {
  @Override
  public Hotel extractData(ResultSet rs) throws SQLException, DataAccessException {
    return Hotel.builder()
        .id(rs.getLong("ID"))
        .name(rs.getString("NAME"))
        .description(rs.getString("DESCRIPTION"))
        .lang(rs.getString("LANG"))
        .ranking(rs.getInt("RANKING"))
        .build();
  }
}
