package org.hillel.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Hotel {

  private Long id;
  private String name;
  private String description;
  private String lang;
  private int ranking;
}
