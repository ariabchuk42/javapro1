package org.hillel;

import org.hillel.service.ArrayCalculatorHandler;
import org.hillel.service.ArrayValueCalculator;

public class Main {
  public static void main(String[] args) {


    ArrayCalculatorHandler arrayCalculatorHandler = new ArrayCalculatorHandler(new ArrayValueCalculator());
    arrayCalculatorHandler.calculate();
  }
}