package org.hillel.service;

import org.hillel.exception.ArrayDataException;
import org.hillel.exception.ArraySizeException;

public class ArrayValueCalculator {

  public ArrayValueCalculator() {
  }

  public int doCalc(String[][] array) throws ArraySizeException, ArrayDataException {
    if (!isArrayValid(array)) {
      throw new ArraySizeException("Sorry, wrong array size!");
    }

    int sum = 0;

    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array[i].length; j++) {
        try {
          sum += Integer.parseInt(array[i][j]);
        } catch (NumberFormatException ex) {
          throw new ArrayDataException(
              "Can't convert string to number in column [" + i + "] in row [" + j + "]!");
        }
      }
    }

    return sum;
  }

  private boolean isArrayValid(String[][] array) {
    for (String[] element : array) {
      if (array.length != element.length) {
        return false;
      }
    }
    return true;
  }

}
