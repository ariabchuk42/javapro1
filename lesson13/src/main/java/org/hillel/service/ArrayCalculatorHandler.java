package org.hillel.service;

import static java.lang.System.out;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import org.hillel.exception.ArrayDataException;
import org.hillel.exception.ArraySizeException;

@AllArgsConstructor
public class ArrayCalculatorHandler {

  private ArrayValueCalculator calculator;

  public void calculate() {

    String[][] validMatrix = {
        {"1", "2", "3", "4"},
        {"5", "6", "7", "8"},
        {"9", "10", "11", "12"},
        {"13", "14", "15", "16"}
    };

    try {
      out.printf("Sum of given array %s elements is: %d%n", Arrays.deepToString(validMatrix),
          calculator.doCalc(validMatrix));
    } catch (NullPointerException ex) {
      out.println("Given array is null!");
    } catch (ArraySizeException | ArrayDataException ex) {
      out.println(ex.getMessage());
    }
  }
}
