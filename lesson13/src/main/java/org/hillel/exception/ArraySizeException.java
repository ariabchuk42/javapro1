package org.hillel.exception;

public class ArraySizeException extends Exception {

  public ArraySizeException(String message) {
    super(message);
  }

}
