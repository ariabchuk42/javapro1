package org.hillel.exception;

public class ArrayDataException extends Exception {

  public ArrayDataException(String message) {
    super(message);
  }
}
