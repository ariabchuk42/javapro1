package org.hillel.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Country {
  private final String name;
}
