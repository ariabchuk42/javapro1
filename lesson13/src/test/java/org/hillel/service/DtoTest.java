package org.hillel.service;

import com.github.javafaker.Faker;
import org.hillel.dto.Car;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DtoTest {

  EasyRandom generator = new EasyRandom();

  Faker faker = new Faker();
  Car car;

  @BeforeEach
  void init() {

    car = generator.nextObject(Car.class);
  }

  @Test
  void testCar() {

    System.out.println(car.getName());
    Assertions.assertNotNull(car.getName());
  }

  @Test
  void testFakeCar() {

    Car car = new Car(faker.cat().name());

    System.out.println(car.getName());
    Assertions.assertNotNull(car.getName());
  }

}
