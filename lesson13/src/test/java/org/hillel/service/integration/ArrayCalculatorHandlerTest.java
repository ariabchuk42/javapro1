package org.hillel.service.integration;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hillel.exception.ArrayDataException;
import org.hillel.exception.ArraySizeException;
import org.hillel.service.ArrayCalculatorHandler;
import org.hillel.service.ArrayValueCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayCalculatorHandlerTest {

  @Test
  void testHandler() throws ArrayDataException, ArraySizeException {

    ArrayValueCalculator arrayValueCalculator = mock(ArrayValueCalculator.class);

    when(arrayValueCalculator.doCalc(any())).thenReturn(-5);

    Assertions.assertDoesNotThrow(() -> new ArrayCalculatorHandler(arrayValueCalculator).calculate());

  }
}
