package org.hillel.service;

import java.util.stream.Stream;
import org.hillel.exception.ArrayDataException;
import org.hillel.exception.ArraySizeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ArrayValueCalculatorTest {

  @Test
  void testCalculation() throws Exception {

    String[][] array = {
        {"1", "2", "3", "4"},
        {"5", "6", "7", "8"},
        {"9", "10", "11", "12"},
        {"13", "14", "15", "16"}
    };

    ArrayValueCalculator calculator = new ArrayValueCalculator();

    int sum = calculator.doCalc(array);

    Assertions.assertEquals(136, sum);
  }

  @ParameterizedTest
  @MethodSource("provideArray")
  void testCalculationArraySizeException(String[][] array) {

    ArrayValueCalculator calculator = new ArrayValueCalculator();

    ArraySizeException arraySizeException =
        Assertions.assertThrows(ArraySizeException.class, () -> calculator.doCalc(array));

    Assertions.assertEquals("Sorry, wrong array size!", arraySizeException.getMessage());
  }

  @Test
  void testCalculationArrayDataException() {

    String[][] array = {
        {"1", "2", "3", "4"},
        {"5", "6", "7", "8"},
        {"9", "10", "11", "12"},
        {"13", "a", "15", "16"}
    };

    ArrayValueCalculator calculator = new ArrayValueCalculator();

    ArrayDataException arraySizeException =
        Assertions.assertThrows(ArrayDataException.class, () -> calculator.doCalc(array));

    Assertions.assertEquals("Can't convert string to number in column [3] in row [1]!", arraySizeException.getMessage());
  }

  private static Stream<Arguments> provideArray() {

    String[][] array1 = {
        {"1", "2", "3", "4", "33"},
        {"5", "6", "7", "8"},
        {"9", "10", "11", "12"},
        {"13", "14", "15", "16"}
    };

    String[][] array2 = {
        {"1", "2", "3", "4"},
        {"5", "6", "7", "8"},
        {"9", "10", "11", "12"},
        {"13", "14", "15", "16"},
        {"17", "18", "19", "120"},
    };

    String[][] array3 = {
        {"1", "2", "3", "4"},
        {"5", "6", "7", "8"},
        {"9", "10", "11", "12"}
    };

    return Stream.of(
        Arguments.of((Object) array1),
        Arguments.of((Object) array2),
        Arguments.of((Object) array3));
  }
}
