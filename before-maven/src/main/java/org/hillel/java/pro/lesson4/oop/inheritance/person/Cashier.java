package org.hillel.java.pro.lesson4.oop.inheritance.person;

public class Cashier extends BookKeeper {

    public Cashier(double salary) {
        super(salary);
    }

    public Cashier(String name, Integer age, double salary) {
        super(name, age, salary);
    }
}
