package org.hillel.java.pro.lesson7.obstacle;

import org.hillel.java.pro.lesson7.participant.Participant;

public interface Obstacle {

    boolean overcome(Participant participant);

}
