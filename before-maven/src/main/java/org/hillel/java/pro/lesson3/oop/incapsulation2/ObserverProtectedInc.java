package org.hillel.java.pro.lesson3.oop.incapsulation2;

import org.hillel.java.pro.lesson3.oop.incapsulation.ProtectedAccess;

public class ObserverProtectedInc extends ProtectedAccess {


    public ObserverProtectedInc() {
        name = "Im new protected class";
    }
}
