package org.hillel.java.pro.lesson8.obstacle;

import org.hillel.java.pro.lesson8.participant.Participant;

public class Track implements Obstacle {

    double distance;

    @Override
    public boolean overcome(Participant participant) {
        return participant.run(distance);
    }
}
