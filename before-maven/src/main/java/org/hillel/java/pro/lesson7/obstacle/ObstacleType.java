package org.hillel.java.pro.lesson7.obstacle;

public enum ObstacleType {

    RUN,
    JUMP
}
