package org.hillel.java.pro.lesson3.oop;

public class Constants {

    private Constants() {}

    public static final String CAT_NAME = "Tom";
    public static final String MOUSE_NAME = "Jerry";

}
