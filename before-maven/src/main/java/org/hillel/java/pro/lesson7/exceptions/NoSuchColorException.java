package org.hillel.java.pro.lesson7.exceptions;

public class NoSuchColorException extends RuntimeException {

    int index;

    public int getIndex() {
        return index;
    }

    public NoSuchColorException(String message, int index) {
        super(message);
        this.index = index;
    }

    public NoSuchColorException(String message, Throwable cause, int index) {
        super(message, cause);
        this.index = index;
    }
}
