package org.hillel.java.pro.lesson6;

public class Main {
    public static void main(String[] args) {

//        char ch = 'a';
//
//        System.out.println(ch);
//
//        byte index = 26;
//        while (index-- > 0) {
//            char tmpCh = (char) (ch + index);
//            System.out.println(tmpCh);
//        }
//
//        char bellSymbol = 7;
//        System.out.println(bellSymbol);


        char unicodeCharFirst = '\u0000';
        char unicodeCharLast = '\uffff';

//        System.out.println(unicodeCharFirst);
//        System.out.println(unicodeCharLast);

        for (int i = unicodeCharFirst; i < unicodeCharLast; i++) {
            char tmp = (char) (unicodeCharFirst + i);
            System.out.print(tmp);
        }
    }
}