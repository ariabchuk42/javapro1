package org.hillel.java.pro.lesson8;

import org.hillel.java.pro.lesson8.obstacle.Wall;
import org.hillel.java.pro.lesson8.participant.Cat;
import org.hillel.java.pro.lesson8.participant.Human;
import org.hillel.java.pro.lesson8.participant.Participant;
import org.hillel.java.pro.lesson8.obstacle.Obstacle;
import org.hillel.java.pro.lesson8.obstacle.Track;

public class Main {

    public static final String PARTICIPANT_FINISHED_COMPETITIONS = "Participant finished competitions";
    private static Obstacle[] obstacles;

    public static void main(String[] args) {

        obstacles = new Obstacle[]{new Track(), new Wall()};
        Participant[] participants = {new Human(), new Cat()};

        runningCompetitions(obstacles, participants, "my message");

    }

    private static void runningCompetitions(Obstacle[] obstacles, Participant[] participants, String message) {
        System.out.println("my message");
        for (Participant participant : participants) {
            for (Obstacle obstacle : obstacles) {
                if (!obstacle.overcome(participant)) {
                    break;
                }
            }
            System.out.println(PARTICIPANT_FINISHED_COMPETITIONS);
        }
    }
}
