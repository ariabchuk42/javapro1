package org.hillel.java.pro.lesson10;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * SocketChannelServer class.
 */
public class SocketChannelServer {

  public static void main(String[] args) throws IOException {

    try (ServerSocketChannel server = ServerSocketChannel.open()) {
      InetSocketAddress address = new InetSocketAddress(8888);
      server.bind(address);

      try (SocketChannel channel = server.accept()) {

        ByteBuffer buffer = ByteBuffer.allocate(512);
        while (channel.read(buffer) > 0) {
          buffer.flip();

          while (buffer.hasRemaining()) {
            System.out.print((char) buffer.get());
          }
          buffer.clear();
        }
      }
    }
  }
}
