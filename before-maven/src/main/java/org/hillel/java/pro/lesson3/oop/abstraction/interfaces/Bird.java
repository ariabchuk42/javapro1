package org.hillel.java.pro.lesson3.oop.abstraction.interfaces;

public interface Bird {



    default void voice() {
        System.out.println("Im a Bird");
    }
}
