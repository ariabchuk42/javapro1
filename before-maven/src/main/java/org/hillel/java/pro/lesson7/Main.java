package org.hillel.java.pro.lesson7;

import org.hillel.java.pro.lesson7.exceptions.ErrorProvider;

public class Main {
    public static void main(String[] args) throws Exception {

//        ExceptionHandling exceptionHandling = new ExceptionHandling();

//        exceptionHandling.printMessage("our message", 3);
//        exceptionHandling.printMessage2("new message", 2);

//        Random random = new Random();
//        ColourProvider colourProvider = new ColourProvider();

//        int i = 0;
//        while (i++ < 6) {
//            String colour = colourProvider.getColour(i);
//            System.out.println(colour);
//            System.out.println("--------------------");
//
//        }

//        while (i++ < 6) {
//            String colour = colourProvider.getColourByIndex(i);
//            System.out.println(colour);
//            System.out.println("--------------------");
//
//        }

        ErrorProvider errorProvider = new ErrorProvider();
//        errorProvider.stackOverflow();
        errorProvider.outOfMemoryError();

    }
}