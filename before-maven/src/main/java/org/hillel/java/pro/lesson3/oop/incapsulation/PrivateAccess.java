package org.hillel.java.pro.lesson3.oop.incapsulation;

public class PrivateAccess {

    private String name;
    private int age;
    private int size;

    public PrivateAccess(String name, int age, int size) {
        this.name = name;
        this.age = age;
        this.size = size;
    }

    public PrivateAccess() {
        this.name = null;
        this.age = 0;
        this.size = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "PrivateAccess{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", size=" + size +
                '}';
    }
}
