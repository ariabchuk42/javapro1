package org.hillel.java.pro.lesson7.obstacle;

import org.hillel.java.pro.lesson7.participant.Participant;

public class Wall implements Obstacle {

    double height;

    @Override
    public boolean overcome(Participant participant) {
        return participant.jump(height);
    }
}
