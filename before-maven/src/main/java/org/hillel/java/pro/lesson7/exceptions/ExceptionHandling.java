package org.hillel.java.pro.lesson7.exceptions;

public class ExceptionHandling {

    public void printMessage(String message, int i) {

        while (i-- > 0) {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            System.out.println(message);
        }

    }

    public void printMessage2(String message, int i) throws InterruptedException {

        while (i-- > 0) {

            Thread.sleep(2000);
            System.out.println(message);
        }

    }

}
