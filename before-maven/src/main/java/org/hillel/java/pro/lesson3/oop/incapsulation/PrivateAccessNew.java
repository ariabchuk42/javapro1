package org.hillel.java.pro.lesson3.oop.incapsulation;

public class PrivateAccessNew {

    private String name;
    private int age;
    private int size;

    public PrivateAccessNew(String name, int age, int size) {
        this.name = name;
        this.age = age;
        this.size = size;
    }

    public PrivateAccessNew() {
        this.name = null;
        this.age = 0;
        this.size = 0;
    }

    public String getName() {
        return name;
    }

    public PrivateAccessNew setName(String name) {
        this.name = name;
        return this;
    }

    public int getAge() {
        return age;
    }

    public PrivateAccessNew setAge(int age) {
        this.age = age;
        return this;
    }

    public int getSize() {
        return size;
    }

    public PrivateAccessNew setSize(int size) {
        this.size = size;
        return this;
    }

    @Override
    public String toString() {
        return "PrivateAccess{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", size=" + size +
                '}';
    }
}
