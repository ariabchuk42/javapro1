package org.hillel.java.pro.lesson3.oop.abstraction.interfaces;

public interface Animal {

    default void voice() {
        System.out.println("I can speak");
    }

    class Details {
        String name;
        String kind;

    }
}
