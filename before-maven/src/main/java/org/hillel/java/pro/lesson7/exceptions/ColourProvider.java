package org.hillel.java.pro.lesson7.exceptions;

public class ColourProvider {

    String[] colours = {"red", "white", "black", "yellow", "green"};

    public String getColour(int index) {
        try {

            if (index > 5)
                throw new NoSuchColorException("no such color exists", index);

            return colours[index];
        } catch (ArrayIndexOutOfBoundsException ex)  {
            System.out.println("Message: " + ex.getMessage());
            System.out.println("Cause: " + ex.getCause());
            ex.printStackTrace();
            return "transparent";
        } catch (NoSuchColorException ex) {
            System.out.println("Message: " + ex.getMessage());
            System.out.println("Cause: " + ex.getCause());
            System.out.println("Index: " + ex.getIndex());
            ex.printStackTrace();
            return "no such color exists";
        }
    }

    public String getColourByIndex(int index) {

        String color = null;

        try {
            if (index > 5)
                throw new NoSuchColorException("no such color exists", index);
            color = colours[index];

            // open file
            // read from file //shit happens

        } catch (ArrayIndexOutOfBoundsException | NoSuchColorException ex) {
            System.out.println("Message: " + ex.getMessage());
            ex.printStackTrace();
            color = "transparent";

        } finally {
            return color;
            // close file
        }
    }

}
