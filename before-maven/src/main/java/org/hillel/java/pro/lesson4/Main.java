package org.hillel.java.pro.lesson4;

import org.hillel.java.pro.lesson4.oop.inheritance.interfaces.EgyptianTriangle;
import org.hillel.java.pro.lesson4.oop.inheritance.interfaces.RightTriangle;
import org.hillel.java.pro.lesson4.oop.inheritance.interfaces.Triangle;
import org.hillel.java.pro.lesson4.oop.inheritance.person.Person;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Person person1 = new Person("Niclas", 1500);
        Person person2 = new Person("Niclas", 1500);
        Person person3 = new Person("Niclas", 60);


//        System.out.println("Are persons equals: ");
//        System.out.println(person1 == person2);
//
//        System.out.println("Are persons equals: ");
//        System.out.println(person1.equals(person2));
//
//        System.out.println("Are persons equals: ");
//        System.out.println(person1 == person3);
//
//        System.out.println("Are persons equals: ");
//        System.out.println(person1.equals(person3));
//
//        System.out.println(person1.hashCode());
//        System.out.println(person2.hashCode());
//        System.out.println(person3.hashCode());


//        System.out.println(person1);



//        System.out.println("---");
//        System.out.println("Audi");
//        Audi audi = new Audi();
//        audi.paint();
//        audi.parking();
//
//        System.out.println("---");
//        System.out.println("Kia");
//        Kia kia = new Kia();
//        kia.paint();
//        kia.parking();
//        kia.clean();
//
//        System.out.println("---");
//        System.out.println("Kia2");
//        Car kia2 = new Kia();
//        kia2.parking();


//        RightTriangle rightTriangle1 = new RightTriangle(5, 7);
//        System.out.println("--------------");
//        System.out.println("Gipotenuza: " + rightTriangle1.getGipotenuza());
//        System.out.println("Perimeter: " + rightTriangle1.getPerimiter());
//        System.out.println("--------------");
//        RightTriangle rightTriangle2 = new RightTriangle(15, 7);
//        System.out.println("Gipotenuza: " + rightTriangle2.getGipotenuza());
//        System.out.println("Perimeter: " + rightTriangle2.getPerimiter());
//        System.out.println("--------------");
//        RightTriangle rightTriangle3 = new RightTriangle(5, 27);
//        System.out.println("Gipotenuza: " + rightTriangle3.getGipotenuza());
//        System.out.println("Perimeter: " + rightTriangle3.getPerimiter());
//        System.out.println("--------------");
//        RightTriangle rightTriangle4 = new RightTriangle(25, 7);
//        System.out.println("Gipotenuza: " + rightTriangle4.getGipotenuza());
//        System.out.println("Perimeter: " + rightTriangle4.getPerimiter());
//        System.out.println("--------------");
//        EgyptianTriangle egyptianTriangle1 = new EgyptianTriangle(3);
//        System.out.println("Gipotenuza: " + egyptianTriangle1.getGipotenuza());
//        System.out.println("Perimeter: " + egyptianTriangle1.getPerimiter());
//        System.out.println("--------------");
//        EgyptianTriangle egyptianTriangle2 = new EgyptianTriangle(6);
//        System.out.println("Gipotenuza: " + egyptianTriangle2.getGipotenuza());
//        System.out.println("Perimeter: " + egyptianTriangle2.getPerimiter());
//        System.out.println("--------------");
//        EgyptianTriangle egyptianTriangle3 = new EgyptianTriangle(10);
//        System.out.println("Gipotenuza: " + egyptianTriangle3.getGipotenuza());
//        System.out.println("Perimeter: " + egyptianTriangle3.getPerimiter());

//        List<RightTriangle> rightTriangles = new ArrayList<>();
//        rightTriangles.add(new RightTriangle(5, 7));
//        rightTriangles.add(new RightTriangle(15, 7));
//        rightTriangles.add(new RightTriangle(5, 27));
//        rightTriangles.add(new RightTriangle(25, 7));
//        rightTriangles.add(new RightTriangle(33, 7));
//        rightTriangles.add(new RightTriangle(25, 27));
//        rightTriangles.add(new RightTriangle(25, 74));
//        List<EgyptianTriangle> egyptianTriangles = new ArrayList<>();
//        egyptianTriangles.add(new EgyptianTriangle(3));
//        egyptianTriangles.add(new EgyptianTriangle(6));
//        egyptianTriangles.add(new EgyptianTriangle(10));
//
//        rightTriangles.forEach(t -> {
//            System.out.println("--------------");
//            System.out.println("Gipotenuza: " + t.getGipotenuza());
//            System.out.println("Perimeter: " + t.getPerimiter());
//        });
//
//        egyptianTriangles.forEach(t -> {
//            System.out.println("--------------");
//            System.out.println("Gipotenuza: " + t.getGipotenuza());
//            System.out.println("Perimeter: " + t.getPerimiter());
//        });


        List<Triangle> triangles = new ArrayList<>();
        triangles.add(new RightTriangle(5, 7));
        triangles.add(new RightTriangle(15, 7));
        triangles.add(new RightTriangle(5, 27));
        triangles.add(new RightTriangle(25, 7));
        triangles.add(new RightTriangle(33, 7));
        triangles.add(new RightTriangle(25, 27));
        triangles.add(new RightTriangle(25, 74));
        triangles.add(new EgyptianTriangle(3));
        triangles.add(new EgyptianTriangle(6));
        triangles.add(new EgyptianTriangle(10));

        triangles.forEach(t -> {
            System.out.println("--------------");
            System.out.println("Gipotenuza: " + t.getGipotenuza());
            System.out.println("Perimeter: " + t.getPerimiter());
        });

    }
}