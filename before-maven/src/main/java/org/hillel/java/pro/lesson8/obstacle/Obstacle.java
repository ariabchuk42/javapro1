package org.hillel.java.pro.lesson8.obstacle;

import org.hillel.java.pro.lesson8.participant.Participant;

public interface Obstacle {

    boolean overcome(Participant participant);

}
