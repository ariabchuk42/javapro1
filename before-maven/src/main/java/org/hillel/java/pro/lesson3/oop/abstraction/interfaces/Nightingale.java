package org.hillel.java.pro.lesson3.oop.abstraction.interfaces;

public class Nightingale implements Animal, Bird {
    @Override
    public void voice() {
        Bird.super.voice();
        Animal.super.voice();

    }

}
