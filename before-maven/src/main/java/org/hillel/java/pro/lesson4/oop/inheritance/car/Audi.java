package org.hillel.java.pro.lesson4.oop.inheritance.car;

public class Audi extends Car {
    @Override
    public void paint() {
        System.out.println("Painting Audi to Yellow color");
    }

    public void cleanAudi() {
        System.out.println("Im cleaning Kia");
    }
}
