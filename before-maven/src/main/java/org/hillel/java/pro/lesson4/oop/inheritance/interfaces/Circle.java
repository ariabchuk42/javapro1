package org.hillel.java.pro.lesson4.oop.inheritance.interfaces;

public interface Circle extends Shape {

    double getRadius();
}
