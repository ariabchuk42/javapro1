package org.hillel.java.pro.lesson9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Objects;

public class Main {

  public static void main(String[] args) throws IOException, ClassNotFoundException {
//    io();
//    ioChar();
//    ioStream();
//    file();
    serialization();
  //  printToFile();
  }

  public static void io() throws IOException {

    try (InputStream inputStream = new FileInputStream("from.io");
         OutputStream outputStream = new FileOutputStream("to.io")) {

      byte[] buffer = new byte[4096];
      int read = inputStream.read(buffer);
      while (read != -1) {
        System.out.println(read);
        outputStream.write(buffer, 0, 4096);
        read = inputStream.read(buffer);
      }
    }
  }

  public static void ioChar() throws IOException {

    try (Reader reader = new FileReader("D:/folder/from.io");
         Writer writer = new FileWriter("to2.io")) {

      char[] buffer = new char[4096];
      int read = reader.read(buffer);
      while (read != -1) {
        System.out.println(read);
        writer.write(buffer, 0, 4096);
        read = reader.read(buffer);
      }
    }
  }

  public static void printToFile() throws IOException {

    String str = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

    try (Writer writer = new FileWriter("to3.io")) {

        writer.write(str.toCharArray());

    }
  }

  public static void ioStream() throws IOException {

    try (InputStream inputStream = new FileInputStream("fromUa.io");
         Reader reader = new InputStreamReader(inputStream, "cp1251");
         BufferedReader bufferedReader = new BufferedReader(reader)
    ) {

      String readLine = bufferedReader.readLine();
      while (Objects.nonNull(readLine)) {
        System.out.println(readLine);
        readLine = bufferedReader.readLine();
      }
    }
  }

  public static void file() throws IOException {

    File file = new File("/Users/andrii/hillel/projects/JavaPro/from.io");
    System.out.println("is file: " + file.isFile());
    System.out.println("can execute: " + file.canExecute());
    System.out.println("get path: " + file.getPath());
    System.out.println("can read: " + file.canRead());
    System.out.println("can write: " + file.canWrite());
    System.out.println("exists: " + file.exists());
    System.out.println("abs path: " + file.getAbsolutePath());
    System.out.println("canonical path: " + file.getCanonicalPath());
    System.out.println("free space: " + file.getFreeSpace());
    System.out.println("name: " + file.getName());
    System.out.println("parent: " + file.getParent());
    System.out.println("is directory: " + file.isDirectory());
    System.out.println("is parent directory: " + file.getParentFile().isDirectory());
    System.out.println("last modified: " + file.lastModified());
    System.out.println("is hidden: " + file.isHidden());

  }

  public static void serialization() throws IOException, ClassNotFoundException {

    Country ua = new Country("UA", 12321.5465, 145887);

    System.out.println(ua);

    System.out.println("Serialization");
    try (FileOutputStream outputStream = new FileOutputStream("country.ua");
         ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {

      objectOutputStream.writeObject(ua);

    }

    System.out.println("Deserialization");
    try (FileInputStream inputStream = new FileInputStream("country.ua");
         ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {

      Country deserial = (Country) objectInputStream.readObject();
      System.out.println(deserial);

    }
  }
}
