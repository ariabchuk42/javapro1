package org.hillel.java.pro.lesson8.participant;

/**
 * Participant Class.
 */
public abstract class Participant {

  public static final String PARTICIPANT_PASSED = "Participant passed";
  public static final String PARTICIPANT_CANNOT = "Participant cannot";
  protected double maxDistance;
  protected double maxHeight;


  // @formatter:off
  @SuppressWarnings("checkstyle:Indentation")
  public boolean run(double distance) {
                    if (distance < maxDistance) {
                            System.out.println(PARTICIPANT_PASSED + " distance");
              return true;
                    } else {
              System.out.println(PARTICIPANT_CANNOT + " run so much");
                    return false;
            }
  }
  // @formatter:on

  public boolean jump(double height) {
    if (height < maxHeight) {
      System.out.println(PARTICIPANT_PASSED + " obstacle");
      return true;
    } else {
      System.out.println(PARTICIPANT_CANNOT + " jump so high");
      return false;
    }
  }

}
