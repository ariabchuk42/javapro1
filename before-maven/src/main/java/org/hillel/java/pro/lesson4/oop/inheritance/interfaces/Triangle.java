package org.hillel.java.pro.lesson4.oop.inheritance.interfaces;

public interface Triangle extends Shape {

    double getGipotenuza();
}
