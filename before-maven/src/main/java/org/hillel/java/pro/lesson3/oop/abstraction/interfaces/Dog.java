package org.hillel.java.pro.lesson3.oop.abstraction.interfaces;

public class Dog implements Animal {
    @Override
    public void voice() {
        System.out.println("Gav Gav");
    }


    public void printName() {
        System.out.println("My name is " + generateDetails().name);
    }

    private Details generateDetails() {
        Animal.Details details = new Animal.Details();

        details.name = "Sobaka";
        details.kind = "Z vuhamy";
        return details;
    }

}
