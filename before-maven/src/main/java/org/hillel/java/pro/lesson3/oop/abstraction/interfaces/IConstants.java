package org.hillel.java.pro.lesson3.oop.abstraction.interfaces;

public interface IConstants {
    String CAT_NAME = "Tom";
    String MOUSE_NAME = "Jerry";
}
