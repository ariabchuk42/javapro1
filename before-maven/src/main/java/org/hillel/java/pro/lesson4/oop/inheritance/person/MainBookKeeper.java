package org.hillel.java.pro.lesson4.oop.inheritance.person;

public class MainBookKeeper extends BookKeeper {


    public MainBookKeeper(double salary) {
        super(salary);
    }

    public MainBookKeeper(String name, Integer age, double salary) {
        super(name, age, salary);
    }
}
