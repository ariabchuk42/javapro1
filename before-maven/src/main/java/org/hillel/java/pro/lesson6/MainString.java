package org.hillel.java.pro.lesson6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainString {
    public static void main(String[] args) {

//        String str = "lesson6";
//        str = "lesson7".toLowerCase(Locale.ROOT);

//        String str2 = "lesson6";


//        System.out.println("First: " + str2 == str);
//        System.out.println("Second: " + str2.equals(str));

//        String valueOfInteger = String.valueOf(125);
//        String valueOfDouble = String.valueOf(125.23);
//
//        System.out.println(valueOfInteger);
//        System.out.println(valueOfDouble);
//
//        String format = String.format("Our first message is: ", "hello lesson 6");
//
//        System.out.println(format);

//        String [] strings = {"first", "second", "third", "forth"};
//
//        System.out.println(String.join("->", strings));

//        String strObj = new String("out of string pool");

//        System.out.println(strObj);
//        strObj.intern();
//
//        char t = strObj.charAt(2);
//        System.out.println(strObj.toUpperCase(Locale.ROOT));

//        for (int i = 0; i < strObj.length(); i++) {
//            System.out.println(strObj.toCharArray()[i]);
//        }

//        String concat = strObj.concat(" part of memory");

        //System.out.println(concat);

//        System.out.println(strObj.substring(7));
//        System.out.println(strObj.substring(7, 13));

//        String[] split = strObj.split(" ");
//        for (String st: split) {
//            System.out.println(st);
//        }
//
//        String strTrim = "         many spaces            ";
//        System.out.println(strTrim);
//        System.out.println(strTrim.trim());

        String str = new StringBuilder("String Builder")
                .append(" build")
                .append(" new")
                .append(" string")
                .append(" for")
                .append(" new me")
                .append("!")
                .toString();

//        str = str.replace("me", "you");
//
//        System.out.println(str);
//
//
//        String strBuffer = new StringBuffer("String Buffer")
//                .append(" build")
//                .append(" new")
//                .append(" string")
//                .append(" for")
//                .append(" me")
//                .append("!")
//                .insert(23, "new text")
//                .replace(23, 31, "")
//                .reverse()
//                .toString();
//
//        System.out.println(strBuffer);
//
//        String a = "a";
//        String b = "b";
//        String c = "a" + "b";
//        String d = a + b;
//        System.out.println(c);
//        System.out.println(d);
//
//        a.toLowerCase(Locale.ROOT);

        //str.contains("build");

        Pattern pattern = Pattern.compile("[a-z]+");
        Matcher matcher = pattern.matcher(str);



        while (matcher.find()) {
            System.out.println("we found match");
        }

    }
}