package org.hillel.java.pro.lesson4.oop.inheritance.interfaces;

public class RightTriangle implements Triangle {

    private final double side1;
    private final double side2;

    public RightTriangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public double getPerimiter() {
        return side1 + side2 + getGipotenuza();
    }

    @Override
    public double getGipotenuza() {
        return Math.sqrt(side1 * side1 + side2 * side2);
    }

}
