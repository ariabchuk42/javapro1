package org.hillel.java.pro.lesson3.oop.incapsulation;

public class Observer {

    public Observer() {

        DefaultAccess defaultAccess = new DefaultAccess();
        defaultAccess.name = "Im default access class";
        defaultAccess.getAge();

        PrivateAccess privateAccess = new PrivateAccess();

        PublicAccess publicAccess = new PublicAccess();
        publicAccess.name = "Im public access class";


        ProtectedAccess protectedAccess = new ProtectedAccess();

    }
}
