package org.hillel.java.pro.lesson4.oop.inheritance.car;

public class Kia extends Car {

    private String engin;

    public Kia() {
        this.color = "Pink";
    }

    public Kia(String color) {
        super(color);
    }

    public Kia(String color, String engin) {
        super(color);
        this.engin = engin;
    }

    @Override
    public void paint() {

        this.color = "Red";
        System.out.println("Paining Kia to Black color");
    }

    @Override
    public void parking() {
        System.out.println("Im parking Kia");
    }

    public void clean() {
        System.out.println("Im cleaning Kia");
    }
}
