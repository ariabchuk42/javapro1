package org.hillel.java.pro.lesson10;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Main Class.
 */
public class Main {

  public static void main(String[] args) throws IOException {

    writer(ByteBuffer.wrap("\nJava Pro Lesson10 message".getBytes()));
    reader();
  }

  public static void reader() throws IOException {

    try (RandomAccessFile file = new RandomAccessFile("from.io", "r")) {
      FileChannel channel = file.getChannel();

      ByteBuffer buffer = ByteBuffer.allocate(1024);
      while (channel.read(buffer) > 0) {

        buffer.flip();
        while (buffer.hasRemaining()) {
          System.out.print((char) buffer.get());
        }
        buffer.clear();
      }
    }
  }

  public static void writer(ByteBuffer buffer) throws IOException {

    try (FileChannel fileChannel = FileChannel.open(Paths.get("from.io"), StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
      fileChannel.write(buffer);
    }
  }

}
