package org.hillel.java.pro.lesson8.obstacle;

public enum ObstacleType {

    RUN,
    JUMP
}
