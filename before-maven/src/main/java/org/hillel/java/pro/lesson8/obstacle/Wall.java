package org.hillel.java.pro.lesson8.obstacle;

import org.hillel.java.pro.lesson8.participant.Participant;

public class Wall implements Obstacle {

    double height;

    @Override
    public boolean overcome(Participant participant) {
        return participant.jump(height);
    }
}
