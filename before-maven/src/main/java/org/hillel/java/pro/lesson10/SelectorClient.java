package org.hillel.java.pro.lesson10;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class SelectorClient {

  public static void main(String[] args) throws IOException, InterruptedException {
    try (SocketChannel client = SocketChannel.open(new InetSocketAddress("localhost", 5454))) {

      sendMessage(client, "test");
      Thread.sleep(2000);
      sendMessage(client, "POISON_PILL");
      Thread.sleep(2000);
      sendMessage(client, "after party");
    }
  }

  public static String sendMessage(SocketChannel client, String msg) {
    ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
    String response = null;
    try {
      client.write(buffer);
      buffer.clear();
      client.read(buffer);
      response = new String(buffer.array()).trim();
      System.out.println("response=" + response);
      buffer.clear();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return response;

  }
}
