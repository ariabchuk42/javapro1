package org.hillel.java.pro.lesson3.oop.incapsulation2;

import org.hillel.java.pro.lesson3.oop.incapsulation.PublicAccess;

public class ObserverPublicInc extends PublicAccess {


    public ObserverPublicInc() {

        name = "Im new public class";

    }
}
