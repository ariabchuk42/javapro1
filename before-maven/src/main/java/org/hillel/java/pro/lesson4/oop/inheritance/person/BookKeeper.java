package org.hillel.java.pro.lesson4.oop.inheritance.person;

public class BookKeeper extends Person {

    public double salary;

    public BookKeeper(double salary) {
        this.salary = salary;
    }

    public BookKeeper(String name, Integer age, double salary) {
        super(name, age);
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
