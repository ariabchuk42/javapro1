package org.hillel.java.pro.lesson9;

import java.io.Serializable;

public class Country implements Serializable {

  private String name;
  private transient Double size;
  private Integer population;

  public Country() {
  }

  public Country(String name, Double size, Integer population) {
    this.name = name;
    this.size = size;
    this.population = population;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getSize() {
    return size;
  }

  public void setSize(Double size) {
    this.size = size;
  }

  public Integer getPopulation() {
    return population;
  }

  public void setPopulation(Integer population) {
    this.population = population;
  }

  @Override
  public String toString() {
    return "Country{" +
        "name='" + name + '\'' +
        ", size=" + size +
        ", population=" + population +
        '}';
  }
}
