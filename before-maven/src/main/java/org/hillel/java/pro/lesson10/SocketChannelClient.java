package org.hillel.java.pro.lesson10;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * SocketChannelClient class.
 */
public class SocketChannelClient {

  public static void main(String[] args) throws IOException {

    try (SocketChannel socketChannel = SocketChannel.open()) {

      SocketAddress address = new InetSocketAddress(8888);
      socketChannel.connect(address);

      ByteBuffer buffer = ByteBuffer.wrap("test message".getBytes());
      socketChannel.write(buffer);

      System.out.println("Message sent");

    }
  }
}
