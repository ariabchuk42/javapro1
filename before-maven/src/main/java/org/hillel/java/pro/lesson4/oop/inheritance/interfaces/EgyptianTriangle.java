package org.hillel.java.pro.lesson4.oop.inheritance.interfaces;

public class EgyptianTriangle implements Triangle {

    private final double smallerSide;

    public EgyptianTriangle(double smallerSide) {
        this.smallerSide = smallerSide;
    }

    @Override
    public double getPerimiter() {
        return smallerSide + smallerSide * 4 / 3 + smallerSide * 5 / 3;
    }

    @Override
    public double getGipotenuza() {
        return smallerSide * 5 / 3;
    }
}
