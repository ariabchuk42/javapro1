package org.hillel.java.pro.lesson7.participant;

/**
 * Participant Class.
 */
public abstract class Participant {

  protected double maxDistance;
  protected double maxHeight;


  @SuppressWarnings("checkstyle:MissingJavadocMethod")
  public boolean run(double distance) {
    if (distance < maxDistance) {
      System.out.println("Participant passed distance");
      return true;
    } else {
      System.out.println("Participant cannot run so much");
      return false;
    }
  }

  public boolean jump(double height) {
    if (height < maxHeight) {
      System.out.println("Participant passed obstacle");
      return true;
    } else {
      System.out.println("Participant cannot jump so high");
      return false;
    }
  }

}
