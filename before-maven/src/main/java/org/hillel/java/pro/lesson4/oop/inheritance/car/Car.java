package org.hillel.java.pro.lesson4.oop.inheritance.car;

public abstract class Car {

    protected String color;

    public Car() {
        this.color = "Green";
    }

    public Car(String color) {
        this.color = color;
    }

    public void parking() {
        System.out.println("Im parking Car");
    }

    abstract void paint();

}
