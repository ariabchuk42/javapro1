package org.hillel.java.pro.lesson3.oop.incapsulation3;

public class CatFactory {

    public Cat build() {
        Cat cat = new Cat();
        cat.setAge(getAge());
        cat.setName(getName());
        cat.setFavoriteFood(getFavoriteFood());
        return cat;
    }

    public String getName() {
        return "Murzik";
    }

    public String getFavoriteFood() {
        return "Wiskas";
    }

    public int getAge() {
        return 2;
    }

}
