package org.hillel.java.pro.lesson7.obstacle;

import org.hillel.java.pro.lesson7.participant.Participant;

public class Track2 implements Obstacle {
    double distance;

    @Override
    public boolean overcome(Participant participant) {
        return participant.run(distance);
    }
}
