package org.hillel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Warmup(iterations = 1)
@Measurement(iterations = 4)
@Fork(value = 1, jvmArgs = {"-Xms2G", "-Xmx2G"})
public class BenchmarkAdd {

  @Param({"10000"})
  private int N;

  public static void main(String[] args) throws RunnerException {
    Options opt = new OptionsBuilder()
        .include(BenchmarkAdd.class.getSimpleName())
        .build();

    new Runner(opt).run();
  }

  private List<Integer> DATA_FOR_TESTING;

  public void getList() {

    DATA_FOR_TESTING = new ArrayList<>();
    for (int i = 0; i++ < N;) {
      DATA_FOR_TESTING.add(i);
    }
  }

  @Benchmark
  public void removeFirst(Blackhole bh) {
    getList();

    for (int i = 0; i++ < N;) {
      DATA_FOR_TESTING.remove(i);
      bh.consume(i);
    }
  }

  @Benchmark
  public void arrayList() {
    List<Integer> collection = new ArrayList<>();

    for (int i = 0; i++ < N;) {
      collection.add(i);
    }

  }

  @Benchmark
  public void linkedList() {
    List<Integer> collection = new LinkedList<>();

    for (int i = 0; i++ < N;) {
      collection.add(i);
    }

  }


  @Benchmark
  public void hashSet() {
    Set<Integer> collection = new HashSet<>();

    for (int i = 0; i++ < N;) {
      collection.add(i);
    }

  }

  @Benchmark
  public void treeSet() {
    Set<Integer> collection = new TreeSet<>();

    for (int i = 0; i++ < N;) {
      collection.add(i);
    }
  }

  @Benchmark
  public void linkedHashSet() {
    Set<Integer> collection = new LinkedHashSet<>();

    for (int i = 0; i++ < N;) {
      collection.add(i);
    }

  }



}