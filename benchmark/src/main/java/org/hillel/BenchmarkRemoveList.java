package org.hillel;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class BenchmarkRemoveList {

  public static void main(String[] args) throws RunnerException {
    Options opt = new OptionsBuilder()
        .include(BenchmarkRemoveArrayList.class.getSimpleName())
        .include(BenchmarkRemoveLinkedList.class.getSimpleName())
        .build();

    new Runner(opt).run();
  }
}
