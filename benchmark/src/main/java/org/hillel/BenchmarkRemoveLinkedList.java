package org.hillel;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 1)
@Measurement(iterations = 4)
@Fork(value = 1, jvmArgs = {"-Xms2G", "-Xmx2G"})
@State(Scope.Benchmark)
public class BenchmarkRemoveLinkedList {
  @Param({"1000"})
  public int N;
  private List<Integer> list;


  @Setup(Level.Invocation)
  public void setup() {
    list = getList();
  }

  public List<Integer> getList() {
    List<Integer> list = new LinkedList<>();
    for (int i = 0; i <= N; i++) {
      list.add(i);
    }
    return list;
  }

  @Benchmark
  public void removeFirst() {

    if (N > 0) {
      list.subList(0, N).clear();
    }
  }

  @Benchmark
  public void removeLast() {

    if (N >= 1) {
      list.subList(1, N + 1).clear();
    }
  }
}
