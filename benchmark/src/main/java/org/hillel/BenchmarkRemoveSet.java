package org.hillel;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 1)
@Measurement(iterations = 4)
@Fork(value = 1, jvmArgs = {"-Xms2G", "-Xmx2G"})
@State(Scope.Benchmark)
public class BenchmarkRemoveSet {

  public static void main(String[] args) throws RunnerException {
    Options opt = new OptionsBuilder()
        .include(BenchmarkRemoveSet.class.getSimpleName())
        .build();

    new Runner(opt).run();
  }

  @State(Scope.Benchmark)
  public abstract static class SetState {
    @Param({"1000"})
    public int N;
    public Set<Integer> set;

    @Setup(Level.Invocation)
    public void setup() {
      set = getSet();
    }

    abstract Set<Integer> getSet();
  }

  public static class HashSetState extends SetState {
    public Set<Integer> getSet() {
      Set<Integer> set = new HashSet<>();
      for (int i = 0; i <= N; i++) {
        set.add(i);
      }
      return set;
    }
  }

  public static class TreeSetState extends SetState {
    public Set<Integer> getSet() {
      Set<Integer> set = new TreeSet<>();
      for (int i = 0; i <= N; i++) {
        set.add(i);
      }
      return set;
    }
  }

  public static class LinkedHashSetState extends SetState {
    public Set<Integer> getSet() {
      Set<Integer> set = new LinkedHashSet<>();
      for (int i = 0; i <= N; i++) {
        set.add(i);
      }
      return set;
    }
  }

  @Benchmark
  public void removeFirstFromHashSet(HashSetState state) {

    for (int i = 0; i < state.N; i++) {
      state.set.remove(i);
    }
  }

  @Benchmark
  public void removeFirstFromTreeSet(TreeSetState state) {

    for (int i = 0; i < state.N; i++) {
      state.set.remove(i);
    }
  }

  @Benchmark
  public void removeFirstFromLinkedHashSet(LinkedHashSetState state) {

    for (int i = 0; i < state.N; i++) {
      state.set.remove(i);
    }
  }

  @Benchmark
  public void removeLastFromHashSet(HashSetState state) {

    for (int i = state.N - 1; i >= 0; i--) {
      state.set.remove(i);
    }
  }

  @Benchmark
  public void removeLastFromTreeSet(TreeSetState state) {

    for (int i = state.N - 1; i >= 0; i--) {
      state.set.remove(i);
    }
  }

  @Benchmark
  public void removeLastFromLinkedHashSet(LinkedHashSetState state) {

    for (int i = state.N - 1; i >= 0; i--) {
      state.set.remove(i);
    }
  }
}
