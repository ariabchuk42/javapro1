package org.hillel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.hillel.sort.Sort;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Warmup(iterations = 1)
@Measurement(iterations = 4)
@Fork(value = 1, jvmArgs = {"-Xms2G", "-Xmx2G"})
public class BenchmarkSort {

  @Param({"10000"})
  private int N;

  public static void main(String[] args) throws RunnerException {
    Options opt = new OptionsBuilder()
        .include(BenchmarkSort.class.getSimpleName())
        .build();

    new Runner(opt).run();
  }

  private int[] DATA_FOR_TESTING;

  @Setup
  public void getList() {

    Random random = new Random();

    DATA_FOR_TESTING = new int[N];
    for (int i = 0; i < N; i++) {
      DATA_FOR_TESTING[i] = random.nextInt();
    }
  }

  @Benchmark
  public void bubbleSort() {

    new Sort().bubbleSort(DATA_FOR_TESTING);

  }

  @Benchmark
  public void selectionSort() {


    new Sort().selectionSort(DATA_FOR_TESTING);
  }

  @Benchmark
  public void insertSort() {
    new Sort().insertSort(DATA_FOR_TESTING);
  }


  @Benchmark
  public void cocktailSort() {
    new Sort().cocktailSort(DATA_FOR_TESTING);
  }

  @Benchmark
  public void quickSort() {
    new Sort().quickSort(DATA_FOR_TESTING, 0, DATA_FOR_TESTING.length);
  }

}