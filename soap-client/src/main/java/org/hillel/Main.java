package org.hillel;

import java.util.UUID;
import org.hillel.client.generated.Hotel;
import org.hillel.client.generated.HotelService;
import org.hillel.client.generated.HotelServiceService;

public class Main {
  public static void main(String[] args) {

    System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

    HotelService service = new HotelServiceService().getHotelServicePort();

//    Hotel hotel = new Hotel();
//    hotel.setId(UUID.randomUUID().toString());
//    hotel.setName("Generated hotel");
//    hotel.setDescription("Generated hotel descr");
//    hotel.setLang("UA");
//    hotel.setRanking(5);

    //service.add(hotel);

    service.getById("dc1597fd-8a4f-487b-b161-5aa91b149846");

    service.getAll();

  }
}