package org.hillel.mapper;

import org.hillel.dto.HotelDto;
import org.hillel.entity.Hotel;

public class HotelDemoMapper {

  public Hotel toEntity(HotelDto dto) {
    return Hotel.builder()
        .id(dto.getId())
        .name(dto.getName())
        .description(dto.getDescription())
        .ranking(dto.getRanking())
        .lang(dto.getLang())
        .cityId(dto.getCityId())
        .build();
  }

  public HotelDto toDto(Hotel entity) {
    return HotelDto.builder()
        .id(entity.getId())
        .name(entity.getName())
        .description(entity.getDescription())
        .ranking(entity.getRanking())
        .lang(entity.getLang())
        .cityId(entity.getCityId())
        .build();
  }

}
