package org.hillel.mapper;

import org.hillel.dto.HotelDto;
import org.hillel.entity.Hotel;
import org.mapstruct.Mapper;

@Mapper
public interface HotelMapper {
  Hotel toEntity(HotelDto dto);

  HotelDto toDto(Hotel entity);
}
