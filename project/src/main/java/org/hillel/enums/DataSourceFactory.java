package org.hillel.enums;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.util.Properties;
import javax.sql.DataSource;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.hillel.services.PropertyReader;

@Log4j2
public enum DataSourceFactory {

  INSTANCE;

  private final DataSource dataSource;

  DataSourceFactory() {

    this.dataSource = getDataSource();
  }

  private DataSource getDataSource() {

    Properties properties = new PropertyReader().getProperties("application.properties");
    HikariConfig config = new HikariConfig();

    config.setJdbcUrl(properties.getProperty("db.url"));
    config.setUsername(properties.getProperty("db.user"));
    config.setPassword(properties.getProperty("db.pass"));
    return new HikariDataSource(config);
  }

  @SneakyThrows
  public Connection getConnection() {

    log.debug("Getting connection to DB.");
    return dataSource.getConnection();
  }
}
