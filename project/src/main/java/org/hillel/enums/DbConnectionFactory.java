package org.hillel.enums;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import lombok.SneakyThrows;
import org.hillel.services.PropertyReader;

public enum DbConnectionFactory {

  INSTANCE;

  private final String url;
  private final String user;
  private final String pass;

  DbConnectionFactory() {

    Properties properties = new PropertyReader().getProperties("application.properties");

    this.url = properties.getProperty("db.url");
    this.user = properties.getProperty("db.user");
    this.pass = properties.getProperty("db.pass");
  }

  @SneakyThrows
  public Connection getConnection() {

    return DriverManager.getConnection(this.url, this.user, this.pass);
  }

}
