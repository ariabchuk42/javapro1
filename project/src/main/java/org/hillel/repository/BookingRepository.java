package org.hillel.repository;

import java.util.List;
import java.util.Optional;

public interface BookingRepository<T> {

  List<T> getAll();

  Optional<T> get(Long id);

  void update(T item);

  void add(T item);

  void delete(Long id);

}
