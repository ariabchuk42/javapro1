package org.hillel.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.hillel.entity.Hotel;
import org.hillel.enums.DataSourceFactory;

@Log4j2
public class HotelRepository implements BookingRepository<Hotel> {

  @SneakyThrows
  public List<Hotel> getAll() {
    log.info("Fetching all hotels from DB.");
    String query = "SELECT * FROM hotels";
    log.debug("Query: {}", query);
    List<Hotel> hotels = new ArrayList<>();

    try (Connection connection = DataSourceFactory.INSTANCE.getConnection();
         Statement statement = connection.createStatement()
    ) {

      ResultSet resultSet = statement.executeQuery(query);
      while (resultSet.next()) {

        log.debug("Creating Hotel entity.");
        Hotel hotel = Hotel.builder()
            .id(resultSet.getLong("id"))
            .name(resultSet.getString("name"))
            .description(resultSet.getString("description"))
            .ranking(resultSet.getInt("ranking"))
            .lang(resultSet.getString("lang"))
            .cityId(resultSet.getLong("cityId"))
            .build();


        hotels.add(hotel);
      }
    }
    log.debug("All entities built.");
    return hotels;
  }

  @Override
  public Optional<Hotel> get(Long id) {
    return Optional.empty();
  }

  @Override
  public void update(Hotel item) {

  }

  @Override
  public void delete(Long id) {

  }

  @Override
  @SneakyThrows
  public void add(Hotel hotel) {

    String insert = "INSERT INTO hotels(name, description, ranking, lang, cityId) " +
        "values (?, ?, ?, ?, ?)";

    try (Connection connection = DataSourceFactory.INSTANCE.getConnection();
         PreparedStatement statement = connection.prepareStatement(insert)
    ) {

      statement.setString(1, hotel.getName());
      statement.setString(2, hotel.getDescription());
      statement.setInt(3, hotel.getRanking());
      statement.setString(4, hotel.getLang());
      statement.setLong(5, hotel.getCityId());

      statement.execute();
    }
  }

}
