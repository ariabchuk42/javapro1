package org.hillel.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Hotel {

  private Long id;
  private String name;
  private String description;
  private int ranking;
  private String lang;
  private Long cityId;

}
