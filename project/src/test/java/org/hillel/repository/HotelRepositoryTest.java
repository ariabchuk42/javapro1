package org.hillel.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.hillel.entity.Hotel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@Log4j2
public class HotelRepositoryTest {

  @Test
  void testQuery() {

    new HotelRepository().getAll()
        .forEach(System.out::println);

  }

  @Test
  void testInsert() {

    Hotel hotel = Hotel.builder()
        .name("Hotel Entity2")
        .description("descr 2")
        .ranking(3)
        .cityId(1L)
        .lang("LAT")
        .build();

    new HotelRepository().add(hotel);
  }

  @Test
  void testObjectMapper() throws JsonProcessingException {

    Hotel hotel = Hotel.builder()
        .name("Hotel Entity2")
        .description("descr 2")
        .ranking(3)
        .cityId(1L)
        .lang("LAT")
        .build();

    ObjectMapper objectMapper = new ObjectMapper();

    String strHotel = objectMapper.writeValueAsString(hotel);
    log.info(strHotel);

    Hotel hotelDeser = objectMapper.readValue(strHotel, Hotel.class);

    log.info(hotelDeser);

    Assertions.assertEquals(hotel.getName(), hotelDeser.getName());

  }
}
