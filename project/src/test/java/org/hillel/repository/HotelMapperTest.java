package org.hillel.repository;

import org.hillel.dto.HotelDto;
import org.hillel.entity.Hotel;
import org.hillel.mapper.HotelMapper;
import org.hillel.mapper.HotelMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HotelMapperTest {

  HotelMapper mapper = new HotelMapperImpl();

  @Test
  void testToDto() {
    Hotel hotel = Hotel.builder()
        .name("Hotel Entity2")
        .description("descr 2")
        .ranking(3)
        .cityId(1L)
        .lang("LAT")
        .build();

    HotelDto hotelDto = mapper.toDto(hotel);
    Assertions.assertEquals(hotel.getName(), hotelDto.getName());
  }

  @Test
  void testToEntity() {
    HotelDto hotel = HotelDto.builder()
        .name("Hotel Entity2")
        .description("descr 2")
        .ranking(3)
        .cityId(1L)
        .lang("LAT")
        .build();

    Hotel entity = mapper.toEntity(hotel);
    Assertions.assertEquals(hotel.getName(), entity.getName());
  }
}
