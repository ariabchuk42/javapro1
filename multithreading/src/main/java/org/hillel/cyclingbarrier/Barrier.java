package org.hillel.cyclingbarrier;

import com.github.javafaker.Faker;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.hillel.countdouwnlatch.Student;

public class Barrier {

  public static void main(String[] args) {
    CyclicBarrier barrier = new CyclicBarrier(7, new Course());

    Stream.generate(() -> new Student(barrier))
        .limit(12)
        .forEach(s -> s.start());

  }

  static class Course extends Thread {
    @Override
    public void run() {
      System.out.println("Hillel Java Pro Course is started.");
    }
  }

  @AllArgsConstructor
  static class Student extends Thread {
    CyclicBarrier barrier;

    @Override
    @SneakyThrows
    public void run() {
      System.out.println(new Faker().name().fullName() + " joined");
      barrier.await();
    }
  }

}
