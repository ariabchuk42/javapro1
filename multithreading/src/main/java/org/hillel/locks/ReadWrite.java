package org.hillel.locks;

import com.github.javafaker.Faker;

public class ReadWrite {

  static Person person = new Person("Nick");

  static Faker faker = new Faker();
  public static void main(String[] args) throws InterruptedException {

    new Writer().start();
    new Writer().start();
    Thread.sleep(1000);

    new Reader().start();
    new Reader().start();
    new Reader().start();
    new Reader().start();
    new Reader().start();
  }

  static class Writer extends Thread {

    @Override
    public void run() {
      person.setName(faker.name().fullName());
    }
  }

  static class Reader extends Thread {

    @Override
    public void run() {
      System.out.println(Thread.currentThread().getName() + ": " + person.getName());
    }
  }
}
