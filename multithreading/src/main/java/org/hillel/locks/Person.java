package org.hillel.locks;

import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Person {

  private String name;
  ReadWriteLock lock = new ReentrantReadWriteLock();

  public Person(String name) {
    this.name = name;
    ReadWriteLock lock = new ReentrantReadWriteLock();
  }

  public String getName() {
    lock.readLock().lock();

    System.out.println("Getting person name");
    LocalDateTime to = LocalDateTime.now().plusSeconds(3);

    while (LocalDateTime.now().isBefore(to)) {}

    String n = name;
    lock.readLock().unlock();
    return n;
  }

  public void setName(String name) {
    lock.writeLock().lock();

    System.out.println("Setting person name to " + name);

    LocalDateTime to = LocalDateTime.now().plusSeconds(3);

    while (LocalDateTime.now().isBefore(to)) {}

    this.name = name;

    System.out.println("person name updated");
    lock.writeLock().unlock();
  }
}
