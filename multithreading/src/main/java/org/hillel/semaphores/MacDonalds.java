package org.hillel.semaphores;

import java.util.concurrent.Semaphore;
import java.util.stream.Stream;

public class MacDonalds {

  public static void main(String[] args) {

    Semaphore vilnaKasa = new Semaphore(3);

    Stream.generate(() -> new BurgerEater(vilnaKasa))
        .limit(18)
        .forEach(be -> be.start());
  }
}
