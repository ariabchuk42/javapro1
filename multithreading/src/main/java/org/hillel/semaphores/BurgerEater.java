package org.hillel.semaphores;

import java.util.concurrent.Semaphore;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

@AllArgsConstructor
public class BurgerEater extends Thread {

  Semaphore vilnaKasa;

  @Override
  @SneakyThrows
  public void run() {

    System.out.println(this.getName() + " waiting in queue");
    vilnaKasa.acquire();
    System.out.println(this.getName() + " ordering burger");

    sleep(1000);

    System.out.println(this.getName() + " takeaway");
    vilnaKasa.release();
  }
}
