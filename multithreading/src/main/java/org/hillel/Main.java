package org.hillel;

import lombok.SneakyThrows;
import org.hillel.counter.HRunnable;
import org.hillel.counter.HThread;

public class Main {

  @SneakyThrows
  public static void main(String[] args) {

    new Main().doWork();




    System.out.println(Thread.currentThread().getName());


  }

  @SneakyThrows
  private void doWork() {
    Thread thread1 = new Thread(new Runnable() {
      @Override
      @SneakyThrows
      public void run() {
        for (int i = 0; i < 100; i++) {
          System.out.println("1 hi, im " + Thread.currentThread().getName());
          Thread.sleep(10);

        }
      }
    });

    Thread thread2 = new Thread(() -> {

      for (int i = 0; i < 100; i++) {
        System.out.println("2 hi, Im lambda runnable");
        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }
    });

    Thread thread3 = new HThread();

    Thread thread4 = new Thread(new HRunnable());

    thread1.setPriority(Thread.MAX_PRIORITY);
    thread1.start();

    thread2.setPriority(Thread.MIN_PRIORITY);
    thread2.start();
    thread3.setPriority(Thread.NORM_PRIORITY);
    thread3.start();
    thread4.start();


    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();



  }

}