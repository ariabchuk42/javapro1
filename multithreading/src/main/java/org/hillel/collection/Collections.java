package org.hillel.collection;

import java.util.concurrent.PriorityBlockingQueue;
import lombok.SneakyThrows;

public class Collections {

  @SneakyThrows
  public static void main(String[] args) {

    PriorityBlockingQueue<Object> queue = new PriorityBlockingQueue<>();

    new Thread(() -> {
      try {
        queue.take();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }).start();
    new Thread(() -> queue.add(5)).start();
  }
}
