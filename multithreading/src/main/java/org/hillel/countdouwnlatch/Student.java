package org.hillel.countdouwnlatch;

import com.github.javafaker.Faker;
import java.util.concurrent.CountDownLatch;
import lombok.AllArgsConstructor;

public class Student extends Thread {

  private final CountDownLatch countDownLatch;

  public Student(CountDownLatch countDownLatch) {
    this.countDownLatch = countDownLatch;
    setName(new Faker().name().fullName());
  }

  @Override
  public void run() {
    System.out.println(getName() + " - HomeWork done!");
    countDownLatch.countDown();
  }
}
