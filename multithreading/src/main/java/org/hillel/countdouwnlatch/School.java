package org.hillel.countdouwnlatch;

import java.util.concurrent.CountDownLatch;
import java.util.stream.Stream;
import lombok.SneakyThrows;

public class School {

  @SneakyThrows
  public static void main(String[] args) {

    CountDownLatch count = new CountDownLatch(4);

    Stream.generate(() -> new Student(count))
        .limit(8)
        .forEach(s -> s.start());

    count.await();
    System.out.println("First Homeworks done");

  }
}
