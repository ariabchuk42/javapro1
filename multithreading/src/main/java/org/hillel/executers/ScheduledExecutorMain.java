package org.hillel.executers;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import org.hillel.callable.HillelCallable;

public class ScheduledExecutorMain {

  @SneakyThrows
  public static void main(String[] args) {
    ScheduledExecutorService executorService = Executors.newScheduledThreadPool(4);

    List<Future<Integer>> futures =
        Stream.generate(() -> executorService.schedule(new HillelCallable(), 3, TimeUnit.SECONDS))
            .limit(10)
            .collect(Collectors.toList());

    futures.forEach(f -> print(f));

    executorService.shutdown();

  }

  @SneakyThrows
  public static void print(Future<Integer> val) {
    System.out.println(val.get());
  }
}
