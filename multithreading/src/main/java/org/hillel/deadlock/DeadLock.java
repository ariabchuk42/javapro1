package org.hillel.deadlock;

public class DeadLock {

  public static void main(String[] args) {

    Object lock1 = new Object();
    Object lock2 = new Object();

    HillelThread1 hillelThread1 = new HillelThread1(lock1, lock2);
    HillelThread2 hillelThread2 = new HillelThread2(lock1, lock2);

    hillelThread1.start();
    hillelThread2.start();

  }

}
