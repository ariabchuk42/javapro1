package org.hillel.deadlock;

public class HillelThread1 extends Thread {

  private final Object lock1;
  private final Object lock2;

  public HillelThread1(Object lock1, Object lock2) {
    this.lock1 = lock1;
    this.lock2 = lock2;
  }

  @Override
  public void run() {

    synchronized (lock1) {
      System.out.println("thread 1, lock1");
      synchronized (lock2) {
        System.out.println("thread 1, lock2");
      }
    }
  }

}
