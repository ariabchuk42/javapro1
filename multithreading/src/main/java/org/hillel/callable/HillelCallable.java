package org.hillel.callable;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.Callable;

public class HillelCallable implements Callable<Integer> {

  @Override
  public Integer call() throws Exception {
    System.out.println(Thread.currentThread().getName() + " generating integer value.");

    LocalDateTime to = LocalDateTime.now().plusSeconds(1);

    while (LocalDateTime.now().isBefore(to)) {}

    return new Random().nextInt();
  }
}
