package org.hillel.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import lombok.SneakyThrows;

public class Main {

  @SneakyThrows
  public static void main(String[] args) {

    HillelCallable hillelCallable = new HillelCallable();
    FutureTask<Integer> task = new FutureTask<>(hillelCallable);

    new Thread(task).start();

    System.out.println(task.get());

  }

}
