package org.hillel.counter;

import java.util.concurrent.atomic.AtomicInteger;
import lombok.Getter;

public class Counter2 {

  @Getter
  private AtomicInteger count = new AtomicInteger(0);

  private void increment() {
    count.getAndIncrement();
  }

  public static void main(String[] args) {

    Counter2 counter = new Counter2();
    counter.count();

    System.out.println("main: " + counter.getCount());

  }

  private void count() {

    Thread t1 = new Thread(() -> {
      for (int i = 0; i < 1000000; i++) {
        increment();
      }
      System.out.println("t1: " + count);
    });

    Thread t2 = new Thread(() -> {
      for (int i = 0; i < 1000000; i++) {
        increment();
      }
      System.out.println("t2: " + count);
    });

    t1.start();
    t2.start();

  }
}
