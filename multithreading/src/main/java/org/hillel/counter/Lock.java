package org.hillel.counter;

import lombok.SneakyThrows;

public class Lock {

  private boolean isLocked = false;

  @SneakyThrows
  public synchronized void lock() {
    while (isLocked) {
      wait();
    }
    isLocked = true;
  }

  public synchronized void unlock() {
    isLocked = false;
    notify();
  }

}
