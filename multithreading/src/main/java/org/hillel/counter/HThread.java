package org.hillel.counter;

import lombok.SneakyThrows;

public class HThread extends Thread {

  @Override
  @SneakyThrows
  public void run() {

    for (int i = 0; i < 100; i++) {
      System.out.println("3 hi, Im Hillel Thread");
      Thread.sleep(10);
    }

  }
}
