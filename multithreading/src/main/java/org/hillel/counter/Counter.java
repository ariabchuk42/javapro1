package org.hillel.counter;

import lombok.Getter;

public class Counter {

  @Getter
  private volatile int count = 0;
  private volatile int count2 = 0;


  private final Object monitor = new Object();
  private final Object monitor2 = new Object();

  private void increment() {

    synchronized (monitor) {
      count++;
    }
  }

  private void increment2() {

    synchronized (monitor2) {
      count2++;
    }
  }

  public static void main(String[] args) {

    Counter counter = new Counter();
    counter.count();

    System.out.println("main: " + counter.getCount());

  }

  private void count() {

    Thread t1 = new Thread(() -> {
      for (int i = 0; i < 1000000; i++) {
        increment();
        increment2();
      }
      System.out.println("t1 count1: " + count);
      System.out.println("t1 count2: " + count2);
    });

    Thread t2 = new Thread(() -> {
      for (int i = 0; i < 1000000; i++) {
        increment();
        increment2();
      }
      System.out.println("t2 count1: " + count);
      System.out.println("t2 count2: " + count2);
    });

    t1.start();
    t2.start();

  }
}
