package org.hillel.counter;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import lombok.Getter;

public class Counter4 {

  @Getter
  private volatile int count = 0;

  Lock lock = new ReentrantLock();

  private void increment() {

    lock.lock();

    count++;

    lock.unlock();
  }

  public static void main(String[] args) {

    Counter4 counter = new Counter4();
    counter.count();

    System.out.println("main: " + counter.getCount());

  }

  private void count() {

    Thread t1 = new Thread(contrun);

    Thread t2 = new Thread(contrun);

    t1.start();
    t2.start();

  }

  Runnable contrun = () -> {
    for (int i = 0; i < 1000000; i++) {
      increment();
    }
    System.out.println(Thread.currentThread().getName() + ": " + count);
  };
}
