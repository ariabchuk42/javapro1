package org.hillel.counter;

import lombok.Getter;

public class Counter3 {

  @Getter
  private volatile int count = 0;

  Lock lock = new Lock();

  private void increment() {

    lock.lock();

    count++;

    lock.unlock();
  }

  public static void main(String[] args) {

    Counter3 counter = new Counter3();
    counter.count();

    System.out.println("main: " + counter.getCount());

  }

  private void count() {

    Thread t1 = new Thread(() -> {
      for (int i = 0; i < 1000000; i++) {
        increment();
      }
      System.out.println("t1: " + count);
    });

    Thread t2 = new Thread(() -> {
      for (int i = 0; i < 1000000; i++) {
        increment();
      }
      System.out.println("t2: " + count);
    });

    t1.start();
    t2.start();

  }
}
