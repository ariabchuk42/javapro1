package org.hillel.counter;

import lombok.SneakyThrows;


public class HRunnable implements Runnable {
  @Override
  @SneakyThrows
  public void run() {
    for (int i = 0; i < 100; i++) {
      System.out.println("4 hi, Im Hillel Runnable");
      Thread.sleep(10);
    }
  }
}
