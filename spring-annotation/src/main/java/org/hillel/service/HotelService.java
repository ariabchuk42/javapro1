package org.hillel.service;

import lombok.AllArgsConstructor;
import org.hillel.config.HotelConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HotelService {

  @Autowired
  private HotelConfig config;

  public void sayHello() {
    System.out.println("Hello " + config.getName());
  }

}
