package org.hillel.service;

import lombok.Setter;

@Setter
public class CityService {

  private String name;

  public void printText() {
    System.out.println("Im City Service with name: " + name);

  }
}
