package org.hillel.config;

import org.hillel.service.CityService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.hillel")
public class AppConfig {

  @Bean
  public CityService getCityService() {
    CityService cityService = new CityService();
    cityService.setName("Hillel");
    return cityService;
  }

}
