package org.hillel.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@PropertySource("classpath:application.properties")
public class HotelConfig {

  @Value("${hotel.name}")
  private String name;

}
