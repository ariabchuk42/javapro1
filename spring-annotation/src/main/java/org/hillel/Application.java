package org.hillel;

import javax.xml.ws.Holder;
import org.hillel.config.AppConfig;
import org.hillel.service.CityService;
import org.hillel.service.HotelService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
  public static void main(String[] args) {
    ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

    HotelService service = ctx.getBean(HotelService.class);


    service.sayHello();

    CityService bean = ctx.getBean(CityService.class);
    bean.printText();

  }
}