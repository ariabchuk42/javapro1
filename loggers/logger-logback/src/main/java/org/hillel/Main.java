package org.hillel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger logger = LoggerFactory.getLogger(Main.class);
  public static void main(String[] args) {

    logger.trace("Creating now person");
    Person bogdan = new Person("Bogdan");

    logger.debug("Trying to fetch person name");
    bogdan.getName();


  }
}