package org.hillel;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
public class Person {

//  private static final Logger log = LoggerFactory.getLogger(Person.class);
  private final String name;

  public Person(String name) {

    this.name = name;
    log.info("New Person was created");
    log.debug("Person has {} name.", name);

  }

  public String getName() {
    log.trace("Getting person name");
    return name;
  }
}
