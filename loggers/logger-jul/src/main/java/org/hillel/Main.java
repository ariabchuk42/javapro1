package org.hillel;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import lombok.SneakyThrows;

public class Main {

  @SneakyThrows
  public static void main(String[] args) {

    Logger logger = Logger.getLogger(Main.class.getName());

    LogManager.getLogManager().readConfiguration(Main.class.getClassLoader().getResourceAsStream("logging.properties"));


    logger.severe("SEVERE");
    logger.warning("WARNING");
    logger.info("INFO");
    logger.config("CONFIG");
    logger.fine("FINE");
    logger.finer("FINER");
    logger.finest("FINEST");

    logger.log(Level.INFO, "INFO + ");

  }
}