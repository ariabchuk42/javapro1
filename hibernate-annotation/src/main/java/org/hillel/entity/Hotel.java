package org.hillel.entity;


import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "hotels")
public class Hotel {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String name;
  private String description;
  private String lang;
  private int ranking;

  @ManyToMany
  @JoinTable(
      name = "hotel_visitors",
      joinColumns = { @JoinColumn(name = "hotelId")},
      inverseJoinColumns = { @JoinColumn(name = "visitorId")}
  )
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  private Set<Visitor> visitors;
}
