package org.hillel.repository;

import org.hillel.entity.City;

public class CityRepository extends CrudRepository<City> {
  protected CityRepository() {
    super(City.class);
  }
}
