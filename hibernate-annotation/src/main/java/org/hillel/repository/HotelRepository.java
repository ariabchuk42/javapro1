package org.hillel.repository;

import org.hillel.entity.Hotel;

public class HotelRepository extends CrudRepository<Hotel> {
  protected HotelRepository() {
    super(Hotel.class);
  }
}
