package org.hillel.repository;

import org.hillel.entity.Visitor;

public class VisitorRepository extends CrudRepository<Visitor> {
  protected VisitorRepository() {
    super(Visitor.class);
  }
}
