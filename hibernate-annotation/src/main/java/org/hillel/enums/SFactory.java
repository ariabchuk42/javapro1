package org.hillel.enums;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hillel.entity.City;
import org.hillel.entity.Hotel;
import org.hillel.entity.Visitor;

public enum SFactory {

  INSTANCE;

  private final SessionFactory sessionFactory;

  SFactory() {

    sessionFactory = new Configuration().configure()
        .addAnnotatedClass(Hotel.class)
        .addAnnotatedClass(City.class)
        .addAnnotatedClass(Visitor.class)
        .buildSessionFactory();
  }

  public Session openSession() {
    return sessionFactory.openSession();
  }

}
