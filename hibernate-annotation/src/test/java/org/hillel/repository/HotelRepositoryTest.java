package org.hillel.repository;

import java.util.List;
import org.hillel.entity.Hotel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HotelRepositoryTest {

  CrudRepository<Hotel> repo = new HotelRepository();

  @Test
  void getByIdTest() {

    Hotel hotel = repo.getById(2L);

    Assertions.assertEquals("hilton2", hotel.getName());

    System.out.println(hotel);
  }

  @Test
  void saveHotelTest() {

    Hotel hotel = Hotel.builder()
        .name("hibernate hotel")
        .description("hibernate hotel descr")
        .lang("ENG")
        .ranking(2)
        .build();

    repo.save(hotel);

  }

  @Test
  void updateHotel() {

    Hotel hotel = repo.getById(1L);
    hotel.setName("new name");

    repo.update(hotel);
  }

  @Test
  void deleteHotel() {

    repo.delete(21L);
  }

  @Test
  void getAll() {

    List<Hotel> all = repo.getAll();

    Assertions.assertTrue(all.size() > 5);
  }

}
