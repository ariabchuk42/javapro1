package org.hillel;

import java.util.UUID;

public class ServletUtil {

  public static UUID getUUID() {
    return UUID.randomUUID();
  }

}
