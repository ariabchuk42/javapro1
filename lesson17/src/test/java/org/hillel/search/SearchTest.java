package org.hillel.search;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SearchTest {

  @Test
  void binarySearch() {

    int[] nums = {1, 5, 8, 18, 45, 55, 69, 77};

    Search search = new Search();

    Assertions.assertEquals(0, search.binarySearch(nums, 0, nums.length, 1));
    Assertions.assertEquals(7, search.binarySearch(nums, 0, nums.length, 77));
    Assertions.assertEquals(-1, search.binarySearch(nums, 0, nums.length, 33));
  }
}
