package org.hillel.search;

public class Search {

  public int binarySearch(int[] nums, int min, int max, int target) {

    if (min > max) {
      return -1;
    }

    int mid = (min + max) / 2;

    if (target == nums[mid]) {
      return mid;
    } else if (target < nums[mid]) {
      return binarySearch(nums, min, mid -1, target);
    } else if (target > nums[mid]) {
      return binarySearch(nums, mid + 1, max, target);
    }

    return -1;
  }
}
