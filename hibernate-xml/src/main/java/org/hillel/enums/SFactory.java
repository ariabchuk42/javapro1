package org.hillel.enums;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public enum SFactory {

  INSTANCE;

  private final SessionFactory sessionFactory;

  SFactory() {

    StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
        .configure("hibernate.cfg.xml")
        .build();

    Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

    sessionFactory = metadata.buildSessionFactory();


    //sessionFactory = new Configuration().configure().addFile("resources/Hotel.hbm.xml").buildSessionFactory();
  }

  public Session openSession() {
    return sessionFactory.openSession();
  }

}
