package org.hillel.repository;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.query.criteria.JpaCriteriaQuery;
import org.hillel.entity.Hotel;
import org.hillel.enums.SFactory;

public class HotelRepository {

  public Hotel getById(Long id) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      return session.get(Hotel.class, id);
    }
  }

  public List<Hotel> getAll() {
    try (Session session = SFactory.INSTANCE.openSession()) {

      HibernateCriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
      JpaCriteriaQuery<Hotel> query = criteriaBuilder.createQuery(Hotel.class);
      query.from(Hotel.class);
      return session.createQuery(query).getResultList();
    }
  }

  public void save(Hotel hotel) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      Transaction transaction = session.beginTransaction();
      session.persist(hotel);
      transaction.commit();
    }
  }

  public void update(Hotel hotel) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      Transaction transaction = session.beginTransaction();
      session.update(hotel);
      transaction.commit();
    }
  }

  public void delete(Long id) {
    try (Session session = SFactory.INSTANCE.openSession()) {
      Transaction transaction = session.beginTransaction();
      session.delete(session.get(Hotel.class, id));
      transaction.commit();
    }
  }
}
