package org.hillel.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Hotel {

  private Long id;
  private String name;
  private String description;
  private String lang;
  private int ranking;
}
