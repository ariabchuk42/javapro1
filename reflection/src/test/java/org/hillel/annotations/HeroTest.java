package org.hillel.annotations;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.junit.jupiter.api.Test;

public class HeroTest {

  @Test
  void testHero() {

    Class<Hero> aClass = Hero.class;

    System.out.println(aClass.getCanonicalName());
    System.out.println(aClass.getName());
    System.out.println(aClass.getPackage().getName());
    System.out.println(aClass.getSimpleName());
    System.out.println(aClass.getTypeName());

    System.out.println("-------------------------------");
    for (Constructor constructor : aClass.getConstructors()) {
      System.out.println(constructor.getName());
    }

    for (Field field : aClass.getFields()) {
      System.out.println(field.getName());
    }

    for (Method method : aClass.getMethods()) {
      System.out.println(method.getName());
    }

    System.out.println("-------------------------------");

    for (Constructor constructor : aClass.getDeclaredConstructors()) {
      System.out.println(constructor.getName());
    }

    for (Field field : aClass.getDeclaredFields()) {
      System.out.println(field.getName());
    }

    for (Method method : aClass.getDeclaredMethods()) {
      System.out.println(method.getName());
    }

  }

//  @Test
//  void setHeroName() throws InstantiationException, IllegalAccessException {
//
//    Class<Hero> aClass = Hero.class;
//    Hero hero = aClass.newInstance();
//    for (Field field : aClass.getDeclaredFields()) {
//      SetValue annotation = field.getAnnotation(SetValue.class);
//      String name = annotation.name();
//      field.setAccessible(true);
//      field.set(hero, name);
//    }
//
//    System.out.println("Hero name is: " + hero.getName());
//  }

  @Test
  void reflections() {



  }
}
