package org.hillel.annotations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ReflectionTest {

  @Test
  void testContextCreation() {

    DI di = new DI();
    di.prepareContext();
    di.inject();

    Assertions.assertEquals(4, di.getContext().size());

    Person person = (Person)di.getContext().get(Person.class);
    System.out.println("person car model: " + person.getCar().getModel());
    System.out.println("person ice cream: " + person.getIceCream().getName());

    Hero hero = (Hero)di.getContext().get(Hero.class);
    System.out.println("hero car model: " + hero.getCar().getModel());

  }
}
