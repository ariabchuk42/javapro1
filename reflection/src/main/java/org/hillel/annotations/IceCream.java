package org.hillel.annotations;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Singleton
public class IceCream {

  private String name;
  private boolean isCold;

  public IceCream() {
    name = "plombir";
    isCold = true;
  }
}
