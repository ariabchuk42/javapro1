package org.hillel.annotations;

import lombok.Getter;

@Getter
@Singleton
public class Hero {

  @Inject
  private Car car;

  private String name;

  public Hero() {
    this.name = "kotygoroshko";

  }
}
