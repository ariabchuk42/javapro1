package org.hillel.annotations;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Singleton
public class Car {

  private String model;
  private int age;

  public Car() {
    this.model = "Kia";
    this.age = 8;
  }
}
