package org.hillel.annotations;

import lombok.Getter;
import lombok.Setter;

@Singleton
@Getter
@Setter
public class Person {

  @Inject
  private Car car;

  @Inject
  private IceCream iceCream;

  private String firstName;
  private String lastName;

  public Person() {

    firstName = "first name";
    lastName = "last name";

  }
}
