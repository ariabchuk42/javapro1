package org.hillel.annotations;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import lombok.Getter;
import lombok.SneakyThrows;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;

@Getter
public class DI {

  private Map<Class, Object> context;

  public DI() {
    this.context = new HashMap<>();
  }

  @SneakyThrows
  public void prepareContext() {

    Reflections reflections = new Reflections("org.hillel.annotations");
    Set<Class<?>> singletons = reflections.getTypesAnnotatedWith(Singleton.class);
    for (Class clss : singletons) {
      context.put(clss, clss.newInstance());
    }

  }

  @SneakyThrows
  public void inject() {

    Reflections reflections = new Reflections("org.hillel.annotations");
    Set<Class<?>> singletons = reflections.getTypesAnnotatedWith(Singleton.class);
    for (Class clss : singletons) {
      for (Field field : clss.getDeclaredFields()) {
        Inject annotation = field.getAnnotation(Inject.class);
        if (Objects.isNull(annotation)) {
          continue;
        }
        Class<?> type = field.getType();
        field.setAccessible(true);
        field.set(context.get(clss), context.get(type));
      }
    }

  }

}
