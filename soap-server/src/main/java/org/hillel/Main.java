package org.hillel;

import javax.xml.ws.Endpoint;
import lombok.SneakyThrows;
import org.hillel.service.HotelService;

public class Main {

  private static final String ADDRESS = "http://localhost:9999/hotels?wsdl";
  private static final Object SERVICE = new HotelService();

  public static void main(String[] args) {

    Endpoint endpoint = Endpoint.publish(ADDRESS, SERVICE);


    System.out.println("Endpoint: " + ADDRESS);

    sleep();
    endpoint.stop();

  }

  @SneakyThrows
  private static void sleep() {

    while (true)
      Thread.sleep(1000000);
  }
}