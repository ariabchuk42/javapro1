package org.hillel.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.jws.WebService;
import org.hillel.entity.Hotel;

@WebService
public class HotelService {

  Map<UUID, Hotel> storage = new HashMap<>();

  public void add(Hotel hotel) {
    storage.put(hotel.getId(), hotel);
  }

  public Hotel getById(UUID uuid) {
    return storage.get(uuid);
  }

  public Collection<Hotel> getAll() {
    return storage.values();
  }

}
