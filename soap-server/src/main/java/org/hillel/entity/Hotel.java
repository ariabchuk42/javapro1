package org.hillel.entity;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class Hotel {

  private UUID id;
  private String name;
  private String description;
  private int ranking;
  private String lang;

  public Hotel(String name, String description, int ranking, String lang) {
    this.id = UUID.randomUUID();
    this.name = name;
    this.description = description;
    this.ranking = ranking;
    this.lang = lang;
  }
}
