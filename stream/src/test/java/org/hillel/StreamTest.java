package org.hillel;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StreamTest {

  @Test
  void testStream() {

    List<String> list = Stream.of("1", "2", "3")
        .filter(e -> e.equals("3"))
        .collect(Collectors.toList());


    Assertions.assertEquals(1, list.size());
    Assertions.assertEquals("3", list.get(0));
  }

  @Test
  void testStreamEmpty() {

    Stream<String> stream = Stream.of("1", "2", "3")
        .filter(e -> e.equals("4"));


    Assertions.assertEquals(0, stream.count());

  }

  @Test
  void testStreamMap() {

    List<Integer> list = Stream.of("1", "2", "3")
        .peek(System.out::println)
        .filter(e -> e.equals("3"))
        .peek(System.out::println)
        .map(Integer::valueOf)
        .collect(Collectors.toList());

    Assertions.assertEquals(1, list.size());
    Assertions.assertEquals(3, list.get(0));
  }

  @Test
  void testStreamFlatMap() {

    List<String> list = Stream.of("124", "258", "31")
        .flatMap(e -> Arrays.asList(e.split("")).stream())
        .peek(System.out::println)
        .collect(Collectors.toList());

    Assertions.assertEquals(8, list.size());
  }

  @Test
  void testStreamDistinct() {

    List<Integer> list = Stream.of("1", "3", "2", "3")
        .filter(e -> e.equals("3"))
        .map(Integer::valueOf)
        .distinct()
        .collect(Collectors.toList());

    Assertions.assertEquals(1, list.size());
    Assertions.assertEquals(3, list.get(0));
  }

  @Test
  void testStreamForEach() {

    Stream.of("1", "3", "2", "3")
        .sorted()
        .forEach(System.out::println);

  }

  @Test
  void testStreamCount() {

    long count = Stream.of("1", "3", "2", "3")
        .filter(e -> e.equals("3"))
        .count();

    Assertions.assertEquals(2, count);

  }

  @Test
  void testStreamCollect() {

    Set<String> set = Stream.of("1", "3", "2", "3")
        .collect(Collectors.toSet());

    Assertions.assertEquals(3, set.size());

  }

  @Test
  void testStreamCollectMap() {

    Map<Integer, String> map = Stream.of("1", "3", "2", "3")
        .collect(Collectors.toMap(Integer::valueOf, v -> v, (a, b) -> "kolobok"));

    map.forEach((Integer k, String v) -> System.out.println("key: " + k + ", value: " + v));
    Assertions.assertEquals(3, map.size());
  }

  @Test
  void testStreamReuse() {

    List<String> list = Stream.of("1", "3", "2", "3").collect(Collectors.toList());
    list.forEach(System.out::println);

  }

  @Test
  void testStreamFirst() {

    Stream.of("1", "3", "2", "3").findFirst().ifPresent(System.out::println);
    Stream.of("1", "3", "2", "3").findAny().ifPresent(System.out::println);
    Assertions.assertTrue(Stream.of("1", "3", "2", "3").anyMatch(e -> e.equals("2")));
    Assertions.assertFalse(Stream.of("1", "3", "2", "3").allMatch(e -> e.equals("2")));
    Assertions.assertTrue(Stream.of("1", "3", "2", "3").allMatch(e -> e instanceof String));

  }

  @Test
  void testReduce() {

    Stream.of("1", "3", "2", "3")
        .map(e -> Integer.valueOf(e))
        .reduce((a, b) -> a + b)
        .ifPresent(s -> System.out.println(s));


  }

  @Test
  void testReduce2() {

    Integer sum = Stream.of("1", "3", "2", "3")
        .map(e -> Integer.valueOf(e))
        .reduce(9, (a, b) -> a + b);

    Assertions.assertEquals(18, sum);

  }

  @Test
  void testReduce3() {

    HashMap<Integer, String> hashMap = Stream.of("1", "3", "2", "3")
        .reduce(new HashMap<>(), (map, val) -> {
          map.put(Integer.valueOf(val), val);
          return map;
        }, (map, val) -> map);

    hashMap.forEach((k, v) -> System.out.println("key: " + k + ", value: " + v));


  }

  @Test
  void testIntStream() {

    int sum = Stream.of("1", "3", "2", "3")
        .mapToInt(e -> Integer.valueOf(e))
        .sum();

    Assertions.assertEquals(9, sum);

  }

  @Test
  void testIntStream2() {

    IntStream.range(0, 10).forEach(e -> System.out.println(e));
  }

  @Test
  void testIntStream3() {

    IntStream.rangeClosed(0, 10).forEach(e -> System.out.println(e));
  }

  @Test
  void testIntStream4() {

    List<Integer> list = IntStream.rangeClosed(0, 10).boxed().collect(Collectors.toList());
  }

  @Test
  void testGenerate() {

    Stream.generate(() -> "hello")
        .limit(3)
        .forEach(e -> System.out.println(e));
  }

  @Test
  void testIterate() {

    Stream.iterate(15, i -> i + 5)
        .limit(3)
        .forEach(e -> System.out.println(e));
  }

  @Test
  void testf() {

    //fLoop(10);

    fStream(10);
  }

  void fLoop(int amount) {
    int n1 = 0;
    int n2 = 1;
    System.out.println(n1);
    System.out.println(n2);
    while (amount-- > 2) {
      int n3 = n1 + n2;
      System.out.println(n3);
      n1 = n2;
      n2 = n3;
    }
  }

  void fStream(int amount) {
    Stream.iterate(new int[] {0, 1}, f -> new int[] {f[1], f[0] + f[1]})
        .limit(amount)
        .map(f -> f[0])
        .forEach(f -> System.out.println(f));
  }

}
