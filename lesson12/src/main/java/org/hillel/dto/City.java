package org.hillel.dto;

import lombok.Builder;
import lombok.Getter;

/**
 * City class.
 */
@Getter
@Builder
public class City {

  private final String name;
  private final int population;

}
