package org.hillel;

import lombok.extern.slf4j.Slf4j;
import org.hillel.dto.City;

public class Main {
  public static void main(String[] args) {

    City ny = City.builder()
        .population(35000000)
        .name("NY")
        .build();

//    System.out.println(city.getName());
//    System.out.println(city.getPopulation());

    System.out.println(ny);
  }
}