package org.hillel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.Test;

public class GenericsTest {

//  @Test
//  void test() {
//
//    List list = new ArrayList();
//
//    list.add(1);
//    list.add(2);
//    list.add(4);
//    list.add(6);
//    list.add(8);
//
//    list.add("Ivan");
//
//    int sum = 0;
//    for (Object obj : list) {
//
//      sum += (Integer) obj;
//    }
//
//    System.out.println("sum: " + sum);
//
//  }

  @Test
  void testGenerics() {

    List<Integer> list = new ArrayList<>();

    list.add(1);
    list.add(2);
    list.add(4);
    list.add(6);
    list.add(8);

    //list.add("Ivan");

    int sum = 0;
    for (Integer obj : list) {

      sum += obj;
    }

    System.out.println("sum: " + sum);

  }
}
