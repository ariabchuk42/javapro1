package org.hillel;

import org.hillel.car.BMW;
import org.hillel.car.X5;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoxTest {

  @Test
  void testBox() {

    Box<String> stringBox = new Box<>();

    stringBox.setItem("kolobok");

    Assertions.assertInstanceOf(String.class, stringBox.getItem());

  }

  @Test
  void testContainer() {

    Container<Double> doubleContainer = new Box<>();

    doubleContainer.setItem(2.3);

    Assertions.assertInstanceOf(Double.class, doubleContainer.getItem());

  }

  @Test
  void testContainerObject() {

    Container<Object> objectBox = new Box<>();

    objectBox.setItem(2.3);
    Assertions.assertInstanceOf(Double.class, objectBox.getItem());

    objectBox.setItem("test");
    Assertions.assertInstanceOf(String.class, objectBox.getItem());

  }

  @Test
  void testBigBox() {

    BigBox<String, Long> bigBox = new BigBox<>();
    bigBox.setItem("item1");
    bigBox.setAnotherItem(4L);

    Assertions.assertInstanceOf(String.class, bigBox.getItem());
    Assertions.assertInstanceOf(Long.class, bigBox.getAnotherItem());

  }

  @Test
  void testCarBox() {

    CarBox<BMW> box = new CarBox<>();

    box.setItem(new BMW());
    box.setItem(new X5());

  }

}
