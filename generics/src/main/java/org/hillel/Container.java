package org.hillel;

public interface Container<T> {

  void setItem(T item);

  T getItem();
}
