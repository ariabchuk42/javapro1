package org.hillel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BigBox<T, K> implements Container<T> {

  private T item;
  private K anotherItem;
}
