package org.hillel;

import lombok.Getter;
import lombok.Setter;
import org.hillel.car.Car;

@Setter
@Getter
public class CarBox<T extends Car> implements Container<T> {

  private T item;

  void getInfo() {

    item.drive();

  }

}
