package org.hillel;

public interface Provider {

  <W> W getValue();
}
